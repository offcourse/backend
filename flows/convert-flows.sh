#!/usr/bin/env bash

YAML2JSON=$(which yaml2json)

if [ -f $YAML2JSON ] ; then
  npm install yamljs
  YAML2JSON=./node_modules/yamljs/bin/yaml2json
fi

mkdir target

$YAML2JSON process-bookmark-flow.yaml > target/process-bookmark-flow.json.tpl

$YAML2JSON process-course-flow.yaml > target/process-course-flow.json.tpl

$YAML2JSON import-course-flow.yaml > target/import-course-flow.json.tpl

$YAML2JSON import-repo-flow.yaml > target/import-repo-flow.json.tpl

$YAML2JSON missing-bookmark-flow.yaml > target/missing-bookmark-flow.json.tpl

$YAML2JSON index-courses-flow.yaml > target/index-courses-flow.json.tpl

$YAML2JSON remove-courses-flow.yaml > target/remove-courses-flow.json.tpl

$YAML2JSON debug.yaml > target/debug.json.tpl

$YAML2JSON save-status-flow.yaml > target/save-status-flow.json.tpl
