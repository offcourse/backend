import Query from "./query/index.graphql";
import Mutation from "./mutation/index.graphql";
import Profile from "./types/profile.graphql";
import Course from "./types/course.graphql";
import Checkpoint from "./types/checkpoint.graphql";
import Resource from "./types/resource.graphql";
import Status from "./types/status.graphql";
import PageInfo from "./types/pageInfo.graphql";

const typeDefs = [
  "directive @isAuthenticated on FIELD_DEFINITION",
  Query,
  Mutation,
  Course,
  Checkpoint,
  Resource,
  Profile,
  Status,
  PageInfo
];

export default typeDefs;
