import { courseStore, statusStore } from "../../stores";
import { errors } from "../../helpers";
import { find } from "ramda";

export default (root, { checkpointQuery }, context) => {
  const { goal, curator, task } = checkpointQuery;
  const { userName: learner } = context;
  if (!checkpointQuery) {
    errors.invalidArguments("You need to pass in checkpointQuery");
  }

  if (checkpointQuery) {
    return courseStore
      .findOne({ courseQuery: { goal, curator } })
      .then(course => {
        if (!course) {
          errors.invalidArguments("Course doesn't exist");
        }
        context.course = course;

        const queryTask = task.toLowerCase();
        const checkpoint = find(
          ({ task: cpTask }) => queryTask === cpTask,
          course.checkpoints
        );
        context.checkpoint = checkpoint;

        if (!learner) {
          return { course, ...checkpoint };
        }

        return statusStore
          .get({
            courseId: course.courseId,
            learner
          })
          .then(status => {
            context.status = status;
            return { course, ...checkpoint };
          });
      });
  }
};
