import { courseStore } from "../../stores";
import { getCourseTags, errors } from "../../helpers";

export default (root, { courseQuery, courseId }, context) => {
  if (!courseId && !courseQuery) {
    errors.invalidArguments(
      "You need to pass in either a courseQuery or a courseId"
    );
  }

  if (courseId && courseQuery) {
    errors.invalidArguments(
      "You cannot pass in both a courseId and a courseQuery. Choose one."
    );
  }

  if (courseId) {
    return courseStore
      .get(courseId)
      .then(course => {
        if (course) {
          context.course = { ...course, tags: getCourseTags(course) };
        }
        return course;
      })
      .catch(() => null);
  }

  if (courseQuery) {
    return courseStore.findOne({ courseQuery }).then(course => {
      if (course) {
        context.course = { ...course, tags: getCourseTags(course) };
      }
      return course;
    });
  }
};
