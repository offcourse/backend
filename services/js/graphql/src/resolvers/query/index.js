import courses from './courses';
import course from './course';
import checkpoint from './checkpoint';
import resource from './resource';
import currentUser from './currentUser';
import status from './status';

export default {
  courses,
  course,
  checkpoint,
  resource,
  currentUser,
  status
};
