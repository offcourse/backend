import R from "ramda";
import { courseStore } from "../../stores";
import { errors } from "../../helpers";

export default (
  obj,
  {
    first = 10,
    after,
    tag,
    curator,
    searchTerm,
    baseId,
    parentId,
    collectionName
  }
) => {
  const NumberOfQueryArguments = R.filter(item => item, [
    tag,
    searchTerm,
    collectionName,
    curator,
    baseId,
    parentId
  ]).length;

  if (NumberOfQueryArguments > 1) {
    errors.invalidArguments(
      "You cannot passed in multiple query criteria, choose one."
    );
  }

  return courseStore.find({
    tag,
    curator,
    searchTerm,
    collectionName,
    baseId,
    parentId,
    first,
    after
  });
};
