import { resourceStore } from "../../../stores";

export default (root, { resourceUrl }, context) => {
  return resourceStore.get(resourceUrl).then(resource => {
    context.resource = resource;
    return resource;
  });
};
