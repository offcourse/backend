import { statusStore } from '../../stores';

export default (root, { baseId }, { userName }) => {
  return statusStore.get({
    baseId,
    learner: userName
  });
};
