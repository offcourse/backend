import { courseStore } from "../../stores";
import { getCourseTags, errors } from "../../helpers";

export default (root, { course: courseInput }, context) => {
  const { userName } = context;
  const { courseId, curator, goal } = courseInput;

  if (userName.toLowerCase() !== curator.toLowerCase()) {
    errors.unauthorized(
      "You are not the curator of this course, did you mean to this course fork instead?"
    );
  }

  if (courseId) {
    return courseStore
      .get(courseId)
      .then(previousVersion => {
        context.course = {
          ...previousVersion,
          courseInput,
          tags: getCourseTags(previousVersion)
        };
        return courseStore.update(previousVersion, courseInput);
      })
      .catch(error => {
        if (error.message === "Not Found") {
          return errors.invalidArguments(
            "This courseId does not exist yet. For new courses, leave out the courseId. The server will assign those to you."
          );
        }
        return errors.badConfig(error.message);
      });
  }

  return courseStore
    .findOne({
      courseQuery: {
        goal: goal.toLowerCase(),
        curator: curator.toLowerCase()
      }
    })
    .then(data => {
      if (data) {
        return errors.invalidArguments(
          "You already have a course with this title..."
        );
      }
      return data;
    })
    .then(() => {
      context.course = courseInput;
      return courseStore.create(courseInput);
    });
};
