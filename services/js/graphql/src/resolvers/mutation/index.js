import addCourse from './addCourse';
import forkCourse from './forkCourse';
import updateStatus from './updateStatus';

export default {
  addCourse,
  forkCourse,
  updateStatus
};
