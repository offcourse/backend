import { statusStore, courseStore } from "../../stores";
import { contains } from "ramda";
import { errors } from "../../helpers";

export default (root, { statusUpdate }, context) => {
  const { userName: learner } = context;
  const { courseId, checkpointId } = statusUpdate;

  const coursePromise = courseStore.get(courseId).catch(() => null);
  const statusPromise = statusStore.get({ courseId, learner });

  let course = null;

  return Promise.all([coursePromise, statusPromise])
    .then(([foundCourse, status]) => {
      if (foundCourse && status) {
        context.course = foundCourse;
        return statusStore.update(status, checkpointId).then(status => {
          context.isTaskCompleted = contains(
            checkpointId,
            status.checkpointIds
          );
          return status;
        });
      }

      if (foundCourse) {
        context.course = foundCourse;
        const newStatus = statusStore.create({ courseId, learner });
        return statusStore.update(newStatus, checkpointId).then(status => {
          context.isTaskCompleted = contains(
            checkpointId,
            status.checkpointIds
          );
          return status;
        });
      }

      return errors.invalidArguments(
        "The status references an invalid course-id"
      );
    })
    .then(res => res && context.course);
};
