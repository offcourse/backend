import { courseStore } from "../../stores";
import { getCourseTags, errors } from "../../helpers";

export default (root, { courseId }, context) => {
  const { userName } = context;
  return courseStore
    .get(courseId)
    .then(originalCourse => {
      context.parent = {
        ...originalCourse,
        tags: getCourseTags(originalCourse)
      };
      const { curator } = originalCourse;
      if (userName === curator) {
        return errors.invalidArguments(
          "You already are the author of this course, you cannot fork it silly ;-) "
        );
      }
      return originalCourse;
    })
    .then(originalCourse => {
      const { goal } = originalCourse;
      const courseQuery = { curator: userName, goal };
      return { courseQuery, originalCourse };
    })
    .then(({ courseQuery, originalCourse }) => {
      return courseStore.findOne({ courseQuery }).then(existingFork => {
        if (existingFork) {
          return errors.invalidArguments(
            "You already have a course with this title..."
          );
        }
        return originalCourse;
      });
    })
    .then(originalCourse => courseStore.fork(originalCourse, userName))
    .then(fork => {
      context.fork = fork;
      return fork;
    });
};
