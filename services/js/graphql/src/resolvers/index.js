import Query from './query';
import Mutation from './mutation';
import Course from './types/course';
import Checkpoint from './types/checkpoint';
import Resource from './types/resource';
import Profile from './types/profile';
import ResourceContent from './unions/resourceContent';

const resolvers = {
  Query,
  Mutation,
  Course,
  Profile,
  Checkpoint,
  Resource,
  ResourceContent
};

export default resolvers;
