import { courseStore } from '../../stores';

const profile = {
  courses(root, { first, after }, { userName }) {
    const curator = userName;
    return courseStore.find({ curator, first, after });
  }
};

export default profile;
