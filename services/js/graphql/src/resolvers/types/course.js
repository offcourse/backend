import R from "ramda";
import { statusStore, courseStore } from "../../stores";
import { getCourseTags } from "../../helpers";

const course = {
  base({ baseId }) {
    return courseStore.get(baseId);
  },
  courseUrl({ goal, curator }) {
    return `https://www.offcourse.io/curator/${encodeURIComponent(
      curator
    )}/goal/${encodeURIComponent(goal)}`;
  },
  parent({ parentId }) {
    return courseStore.get(parentId);
  },
  siblings({ courseId, parentId, first, after }, { includeSelf }) {
    return (
      parentId &&
      courseStore.find({
        parentId,
        courseId,
        first,
        after,
        includeSelf
      })
    );
  },
  children({ courseId, first, after }) {
    return courseStore.find({
      parentId: courseId,
      first,
      after
    });
  },
  lineage({ courseId, baseId, first, after }, { includeSelf }) {
    return courseStore.find({
      courseId,
      baseId,
      first,
      after,
      includeSelf
    });
  },
  fork({ goal }, _, { userName: curator }) {
    return courseStore.findOne({
      courseQuery: {
        goal,
        curator
      }
    });
  },
  avatarUrl({ curator }) {
    return `https://api.qrserver.com/v1/create-qr-code/?size=150x150&bgcolor=000&color=fff&data=http://app.offcourse.io/?curator=${curator}`;
  },
  tags({ checkpoints }, _, context) {
    return getCourseTags({ checkpoints });
  },
  status({ courseId }, _, { userName }) {
    return statusStore.get({
      courseId,
      learner: userName
    });
  },
  checkpoint({ courseId, checkpoints }, { task }, ctx) {
    const { userName } = ctx;
    if (!task) {
      return null;
    }
    const checkpoint = R.find(R.propEq("task", task.toLowerCase()))(
      checkpoints
    );

    if (!userName) {
      return checkpoint;
    }

    return statusStore
      .get({
        courseId,
        learner: userName
      })
      .then(status => {
        ctx.status = status;
        return checkpoint;
      });
  },
  checkpoints({ courseId, checkpoints }, _, ctx) {
    const { userName } = ctx;
    if (!userName) {
      return checkpoints;
    }
    return statusStore
      .get({
        courseId,
        learner: userName
      })
      .then(status => {
        ctx.status = status;
        return checkpoints;
      });
  }
};

export default course;
