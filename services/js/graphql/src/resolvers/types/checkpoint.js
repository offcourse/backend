import { resourceStore } from '../../stores';
import { contains } from 'ramda';

const checkpoint = {
  completed: ({ checkpointId }, _, { status }) => {
    if (!status) {
      return false;
    }
    return contains(checkpointId, status.checkpointIds);
  },
  resource({ resourceUrl }) {
    return resourceStore.get(resourceUrl);
  }
};

export default checkpoint;
