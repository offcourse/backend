import TurndownService from 'turndown';
import axios from 'axios';
import marked from 'marked';

const turndownService = new TurndownService();

const removeNonAscii = (str) => {
  if (str === null || str === '') return false;
  str = str.toString();

  return str.replace(/[^\x20-\x7E]/g, '');
};

const removeExtension = (str) => {
  if (str === null || str === '') return false;
  str = str.toString();

  return str.replace(/\.md$/, '');
};

const resource = {
  title({ title }) {
    const temp = removeNonAscii(title);
    return removeExtension(temp);
  },
  description({ description }) {
    return removeNonAscii(description);
  },
  content({ rawContent, title, resourceUrl }) {
    if (rawContent['video-provider'] && rawContent['video-id']) {
      return {
        videoProvider: rawContent['video-provider'],
        videoId: rawContent['video-id']
      };
    }

    const gistRE = /http.?:\/\/gist.github.com\/(.+)\/(.+)$/;
    const markdownRE = /.+\.md$/;
    const isMarkdown = title.match(markdownRE);

    const gist = resourceUrl.match(gistRE);
    if (gist && isMarkdown) {
      const [_, gistUserName, gistId] = gist;
      const url = `https://gist.githubusercontent.com/${gistUserName}/${gistId}/raw`;
      return axios.get(url).then((res) => {
        return {
          html: marked(res.data),
          markdown: res.data
        };
      });
    }

    return {
      html: rawContent['html'],
      markdown: turndownService.turndown(rawContent['html'])
    };
  }
};

export default resource;
