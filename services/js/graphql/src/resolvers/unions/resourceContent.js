export default {
  __resolveType(obj) {
    if (obj.html) {
      return 'HTML';
    }

    if (obj.videoId && obj.videoProvider) {
      return 'Video';
    }

    return null;
  }
};
