import { S3 } from 'aws-sdk';
import { errors } from '../helpers';

const instance = new S3();

const parseResponse = ({ Body }) => {
  const bodyJSON = Body.toString();
  const item = JSON.parse(bodyJSON);
  return item;
};

const get = (query) => {
  return new Promise((resolve, reject) => {
    instance.getObject(query, (error, res) => {
      if (error && error.name === 'NoSuchKey') {
        return resolve();
      }

      if (error) {
        return errors.badConfig('S3 error. Did you set the necessary environment variables');
      }

      const parsedResponse = parseResponse(res);
      return resolve(parsedResponse);
    });
  });
};

const put = (query) => {
  return new Promise((resolve, reject) => {
    instance.putObject(query, (error, res) => {
      if (error) {
        return errors.badConfig('S3 error. Did you set the necessary environment variables');
      }
      return resolve(res);
    });
  });
};

export default { put, get };
