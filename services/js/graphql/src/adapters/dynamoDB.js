import { DynamoDB } from "aws-sdk";
import { errors } from "../helpers";

const instance = new DynamoDB.DocumentClient();

const get = query => {
  return new Promise((resolve, reject) => {
    instance.query(query, (error, res) => {
      if (error) {
        return errors.badConfig(
          "DynamoDB error. Did you set the necessary environment variables"
        );
      }
      const item = res.Items[0];
      return resolve(item);
    });
  });
};

const put = query => {
  return new Promise((resolve, reject) => {
    instance.put(query, (error, res) => {
      if (error) {
        return errors.badConfig(
          "DynamoDB error. Did you set the necessary environment variables"
        );
      }
      return resolve(res);
    });
  });
};

export default { put, get };
