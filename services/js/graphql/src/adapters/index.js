import index from './elasticsearch';
import dataLake from './s3';
import DB from './dynamoDB';

export { DB, index, dataLake };
