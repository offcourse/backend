import AWS from 'aws-sdk';
import R from 'ramda';
import HttpAwsEs from 'http-aws-es';

AWS.config.update({
  region: 'us-east-1'
});

const es = require('elasticsearch').Client({
  hosts: [process.env['search_cluster']],
  connectionClass: HttpAwsEs
});

const search = (query) => {
  return es.search(query).then((data) => {
    const items = R.map(item => item._source, data.hits.hits);
    const totalCount = data.hits.total;
    return { items, totalCount };
  });
};

const get = (query) => {
  return es.get(query);
};

export default { get, search };
