import { verbs, objects } from "../constants";
const { COLLECTION } = objects;
import { isEmpty, isNil, both, complement } from "ramda";
const { VIEWED } = verbs;

const isNilnorEmpty = both(complement(isNil), complement(isEmpty));

const overviewQuery = ({ courses }) => {
  if (!isNilnorEmpty(courses)) {
    return null;
  }

  const { ...pageInfo } = courses.pageInfo;

  return {
    verb: VIEWED,
    object: {
      type: COLLECTION,
      ...pageInfo
    }
  };
};

export default overviewQuery;
