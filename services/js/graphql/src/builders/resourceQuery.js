import { verbs, objects } from "../constants";
const { VIEWED } = verbs;
const { RESOURCE } = objects;

const resourceQuery = ({ resourceUrl, resource = {} }) => {
  if (!resourceUrl) {
    return null;
  }
  const { resourceType, description, title, tags } = resource;
  return {
    verb: VIEWED,
    object: {
      type: RESOURCE,
      resourceUrl,
      description,
      resourceType,
      title,
      tags
    }
  };
};

export default resourceQuery;
