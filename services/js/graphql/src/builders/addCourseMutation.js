import { verbs, objects } from "../constants";

const { CREATED, UPDATED } = verbs;
const { COURSE } = objects;

const createdStatement = ({ addCourse, course }) => {
  if (!addCourse) {
    return null;
  }

  const { courseId, revision } = addCourse;
  const { goal, curator, tags, parentId, baseId } = course;
  return {
    verb: revision === 0 ? CREATED : UPDATED,
    object: {
      type: COURSE,
      tags: tags || [],
      goal,
      curator,
      revision: revision || 0,
      courseId: courseId || course.courseId,
      parentId,
      baseId: baseId || courseId
    }
  };
};

export default createdStatement;
