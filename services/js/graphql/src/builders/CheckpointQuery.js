import { verbs, objects } from "../constants";

const { VIEWED } = verbs;
const { CHECKPOINT } = objects;

const checkpointQuery = ({ checkpointQuery, course, checkpoint }) => {
  if (!checkpointQuery || !course || !checkpoint) {
    return null;
  }
  const { courseId, parentId, baseId, revision } = course;
  return {
    verb: VIEWED,
    object: {
      type: CHECKPOINT,
      ...checkpointQuery,
      ...checkpoint,
      revision,
      courseId,
      parentId,
      baseId
    }
  };
};

export default checkpointQuery;
