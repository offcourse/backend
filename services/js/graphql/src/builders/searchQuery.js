import { verbs, objects } from "../constants";
const { SEARCHED } = verbs;
const { COLLECTION } = objects;

const searchQuery = ({ searchTerm, courses }) => {
  if (!searchTerm) {
    return null;
  }

  const { __typename, ...pageInfo } = courses.pageInfo;

  return {
    verb: SEARCHED,
    object: {
      type: COLLECTION,
      searchTerm,
      ...pageInfo
    }
  };
};

export default searchQuery;
