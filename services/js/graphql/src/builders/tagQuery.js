import { verbs, objects } from "../constants";
const { VIEWED } = verbs;
const { COLLECTION } = objects;

const tagQuery = ({ tag, courses }) => {
  if (!tag) {
    return null;
  }

  const { __typename, ...pageInfo } = courses.pageInfo;

  return {
    verb: VIEWED,
    object: {
      type: COLLECTION,
      tag,
      ...pageInfo
    }
  };
};

export default tagQuery;
