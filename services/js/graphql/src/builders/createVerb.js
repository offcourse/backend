const createVerb = ({ variables }) => {
  const { searchterm, statusUpdate } = variables;
  if (searchterm) {
    return {
      id: "https://w3id.org/xapi/dod-isd/verbs/searched",
      display: {
        "en-US": "searched"
      }
    };
  }
  if (statusUpdate) {
    return {
      id: "https://w3id.org/xapi/dod-isd/verbs/completed",
      display: {
        "en-US": "completed"
      }
    };
  }
  return {
    id: "http://id.tincanapi.com/verb/viewed",
    display: {
      "en-US": "viewed"
    }
  };
};

export default createVerb;
