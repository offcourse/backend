const ANONYMOUS = "___ANONYMOUS___";

const createActor = ({ authorizer, identity }) => {
  return {
    account: {
      name: authorizer.username || ANONYMOUS,
      homePage: "https://offcourse.io"
    },
    objectType: "Agent"
  };
};

export default createActor;
