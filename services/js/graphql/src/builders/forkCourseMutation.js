import { verbs, objects } from "../constants";
import { map, filter, uniq, identity, flatten } from "ramda";

const { FORKED } = verbs;
const { COURSE } = objects;

const forkedStatement = ({ forkCourse, parent, fork }) => {
  if (!forkCourse) {
    return null;
  }

  const { courseId: forkId } = fork;
  const { goal, courseId, baseId, curator, revision, tags } = parent;
  return {
    verb: FORKED,
    object: {
      type: COURSE,
      tags,
      goal,
      curator,
      revision,
      courseId,
      baseId,
      forkId
    }
  };
};

export default forkedStatement;
