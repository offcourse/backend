import {
  mergeDeepRight,
  find,
  toPairs,
  reduce,
  isEmpty,
  isNil,
  both,
  complement
} from "ramda";

import updateStatusMutation from "./updateStatusMutation";
import addCourseMutation from "./addCourseMutation";
import forkCourseMutation from "./forkCourseMutation";

import searchQuery from "./searchQuery";
import tagQuery from "./tagQuery";
import curatorQuery from "./curatorQuery";
import overviewQuery from "./overviewQuery";
import courseQuery from "./courseQuery";
import checkpointQuery from "./CheckpointQuery";
import resourceQuery from "./resourceQuery";

import { actors } from "../constants";

export {
  addCourseMutation,
  forkCourseMutation,
  updateStatusMutation,
  searchQuery,
  curatorQuery,
  overviewQuery,
  courseQuery,
  checkpointQuery,
  resourceQuery,
  tagQuery
};

const { ANONYMOUS } = actors;

const isNilnorEmpty = both(complement(isNil), complement(isEmpty));
const chooseOption = find(isNilnorEmpty);

const invalidOperations = ({ errors }) => {
  if (!errors) {
    return null;
  }
  return {};
};

const getViewer = (acc, [key, val]) => {
  const test = key.match(/CloudFront-Is-(.+)-.*/);
  return test && val === "true" ? test[1].toUpperCase() : acc;
};

const createStatement = ({ context, data, errors }) => {
  const {
    course,
    checkpoint,
    parent,
    fork,
    operationName,
    userName,
    role,
    sourceIp,
    timestamp,
    event,
    resource,
    isTaskCompleted,
    variables
  } = context;

  const {
    resourceUrl,
    searchTerm,
    tag,
    courseQuery: cq,
    checkpointQuery: cpq,
    curator,
    statusUpdate
  } = variables;

  const { updateStatus, courses, addCourse, forkCourse } = data;

  const { headers } = event;

  const base = {
    actor: {
      userName: userName || ANONYMOUS,
      role
    },
    context: {
      sourceIp,
      viewer: reduce(getViewer, "", toPairs(headers)),
      country: headers["CloudFront-Viewer-Country"]
    },
    timestamp
  };

  const overrides = [
    invalidOperations({ errors }),
    courseQuery({ courseQuery: cq, course }),
    checkpointQuery({ checkpointQuery: cpq, course, checkpoint }),
    resourceQuery({ resourceUrl, resource }),
    searchQuery({ searchTerm, courses }),
    tagQuery({ tag, courses }),
    curatorQuery({ curator, courses }),
    overviewQuery({ courses }),
    updateStatusMutation({ statusUpdate, course, isTaskCompleted }),
    addCourseMutation({ addCourse, course }),
    forkCourseMutation({ forkCourse, parent, fork })
  ];

  const _statement = chooseOption(overrides) || {};
  const statement = mergeDeepRight(base, _statement);
  console.log(statement);
  return statement;
};

export default createStatement;
