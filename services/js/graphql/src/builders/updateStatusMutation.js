import { find, propEq } from "ramda";
import { verbs, objects } from "../constants";
const { COMPLETED } = verbs;
const { CHECKPOINT } = objects;

const completedStatement = ({ statusUpdate, course, isTaskCompleted }) => {
  if (statusUpdate && course) {
    const { checkpoints, baseId, parentId, curator, goal } = course;
    const { courseId, checkpointId } = statusUpdate;
    const { task, tags } = find(
      propEq("checkpointId", checkpointId),
      checkpoints
    );

    return {
      verb: COMPLETED,
      object: {
        type: CHECKPOINT,
        goal,
        curator,
        task,
        courseId,
        baseId,
        parentId,
        checkpointId,
        tags
      },
      result: { completion: isTaskCompleted }
    };
  }
};

export default completedStatement;
