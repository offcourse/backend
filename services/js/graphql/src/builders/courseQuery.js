import { verbs, objects } from "../constants";

const { VIEWED } = verbs;
const { COURSE } = objects;

const courseQuery = ({ courseQuery, course }) => {
  if (!courseQuery || !course) {
    return null;
  }
  const { courseId, parentId, baseId, revision, tags } = course;
  return {
    verb: VIEWED,
    object: {
      type: COURSE,
      tags,
      ...courseQuery,
      revision,
      courseId,
      parentId,
      baseId
    }
  };
};

export default courseQuery;
