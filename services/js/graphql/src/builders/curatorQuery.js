import { verbs, objects } from "../constants";

const { VIEWED } = verbs;
const { COLLECTION } = objects;

const curatorQuery = ({ curator, courses }) => {
  if (!curator) {
    return null;
  }

  const { __typename, ...pageInfo } = courses.pageInfo;

  return {
    verb: VIEWED,
    object: {
      type: COLLECTION,
      curator,
      ...pageInfo
    }
  };
};

export default curatorQuery;
