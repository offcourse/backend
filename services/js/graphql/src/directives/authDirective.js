import { SchemaDirectiveVisitor } from "apollo-server-lambda";

export default class AuthDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field, details) {
    const { resolve = defaultFieldResolver } = field;
    field.resolve = async function(...args) {
      const { userName, isAuthenticated } = args[2];
      if (!userName || !isAuthenticated) {
        throw new Error("not authorized");
      }
      return resolve.apply(this, args);
    };
  }
}
