import AuthDirective from "./authDirective";

export default { isAuthenticated: AuthDirective };
