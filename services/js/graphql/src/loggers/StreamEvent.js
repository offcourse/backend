import { GraphQLExtension } from "graphql-extensions";
import createStatement from "../builders";
import { curry, identity } from "ramda";

export default class StreamEventLogger extends GraphQLExtension {
  constructor(args) {
    super(args);
    const { streamName, logFunction } = args;
    this.logFunction = curry(logFunction)(streamName);
  }
  requestDidStart({ operationName, variables, context, ...rest }) {
    context.operationName = operationName;
    context.variables = variables;
  }

  willSendResponse({ graphqlResponse, context }) {
    const { errors, data } = graphqlResponse;
    const statement = createStatement({ context, data, errors });
    this.logFunction(statement);
  }
}
