import { GraphQLExtension } from "graphql-extensions";
import { curry, identity } from "ramda";

export default class RawEventLogger extends GraphQLExtension {
  constructor(args) {
    super(args);
    const { streamName, logFunction } = args;
    this.logFunction = curry(logFunction)(streamName);
  }
  requestDidStart({ context }) {
    const { event } = context;
    this.logFunction(event);
  }
}
