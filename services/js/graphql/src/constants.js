const VIEWED = "VIEWED";
const SEARCHED = "SEARCHED";
const COMPLETED = "COMPLETED";
const CREATED = "CREATED";
const UPDATED = "UPDATED";
const FORKED = "UPDATED";

const verbs = { VIEWED, SEARCHED, COMPLETED, CREATED, UPDATED, FORKED };

const COLLECTION = "COLLECTION";
const COURSE = "COURSE";
const CHECKPOINT = "CHECKPOINT";
const RESOURCE = "RESOURCE";

const objects = { COLLECTION, COURSE, CHECKPOINT, RESOURCE };

const ANONYMOUS = "ANONYMOUS";
const actors = { ANONYMOUS };

export { verbs, objects, actors };
