import { ApolloServer } from "apollo-server-lambda";
import schemaDirectives from "./directives";
import typeDefs from "./schema";
import resolvers from "./resolvers";
import { Firehose } from "aws-sdk";
import { StreamEventLogger, RawEventLogger } from "./loggers";
import { errors } from "./helpers";

const firehose = new Firehose();
function put(streamName, io) {
  return new Promise((resolve, reject) => {
    const params = {
      DeliveryStreamName: streamName,
      Record: {
        Data: Buffer.from(`${JSON.stringify(io)}\n`)
      }
    };

    firehose.putRecord(params, (error, res) => {
      if (error) {
        console.log(error);
        return errors.badConfig(
          "firehose error. Did you set the necessary environment variables"
        );
      }
      console.log(streamName, JSON.stringify(res, null, 2));

      return resolve(res);
    });
  });
}

const server = new ApolloServer({
  typeDefs,
  resolvers,
  schemaDirectives,
  extensions: [
    () =>
      new RawEventLogger({
        logFunction: put,
        // streamName: "offcourse-raw-events"
        streamName: process.env["raw_events_stream"]
      }),
    () =>
      new StreamEventLogger({
        logFunction: put,
        // streamName: "offcourse-stream-events"
        streamName: process.env["processed_events_stream"]
      })
  ],
  context: ({ context: lambdaContext, event }) => {
    lambdaContext.callbackWaitsForEmptyEventLoop = true;
    const {
      authorizer,
      identity: ident,
      requestTimeEpoch: timestamp
    } = event.requestContext;
    const { username: userName, role, authenticated } = authorizer;
    const { sourceIp } = ident;
    return {
      event,
      userName,
      sourceIp,
      role,
      timestamp,
      isAuthenticated: authenticated === "true"
    };
  }
});

exports.handler = (event, context, callback) => {
  console.log(JSON.stringify(event, null, 2));
  const handler = server.createHandler({
    cors: {
      origin: "*",
      credentials: true
    }
  });

  return handler(event, context, callback);
};
