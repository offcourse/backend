import { createError } from 'apollo-errors';

const ArgumentsError = createError('ArgumentsError', {
  message: 'You passed in invalid arguments'
});

const AuthorizationError = createError('AuthorizationError', {
  message: 'You are not authorized to do this'
});

const ConfigurationError = createError('ConfigurationError', {
  message: "There's something wrong with your configuration"
});

const invalidArguments = (reason) => {
  throw new ArgumentsError({
    data: {
      reason
    }
  });
};

const unauthorized = (reason) => {
  throw new AuthorizationError({
    data: {
      reason
    }
  });
};

const badConfig = (reason) => {
  throw new ConfigurationError({
    data: {
      reason
    }
  });
};

export default { badConfig, unauthorized, invalidArguments };
