import crypto from 'crypto';

const hasher = (resourceUrl) => {
  return crypto
    .createHmac('sha256', 'offcourse')
    .update(resourceUrl)
    .digest('hex');
};

export { hasher };
