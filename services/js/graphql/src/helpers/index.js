import R from "ramda";
import paginate from "./paginate";
import errors from "./errors";
import { hasher } from "./crypto";

const getCourseTags = ({ checkpoints }) => {
  return R.filter(
    R.identity,
    R.uniq(R.flatten(R.map(({ tags }) => tags, checkpoints)))
  );
};
export { getCourseTags, hasher, paginate, errors };
