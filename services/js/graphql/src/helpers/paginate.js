const R = require('ramda');
const atob = require('atob');

const mapIndexed = R.addIndex(R.map);

const convertNodeIdToCursor = (node) => {
  return bota(node.id.toString());
};

function bota(input) {
  return new Buffer(input.toString(), 'binary').toString('base64');
}

const convertCursorToNodeId = (cursor) => {
  return parseInt(atob(cursor), 10);
};

const createPageInfo = (converter, rawItems, after, totalCount) => {
  const offset = after ? convertCursorToNodeId(after) + 1 : 0;
  const startIndex = offset;
  const endIndex = offset + (rawItems.length - 1);
  const items = R.map(rawItem => converter(rawItem), rawItems);

  const edges = mapIndexed((item, index) => {
    return {
      cursor: convertNodeIdToCursor({ id: index + offset }),
      node: item
    };
  }, items);

  const pageInfo = {
    hasNextPage: endIndex < totalCount - 1,
    endCursor: convertNodeIdToCursor({ id: endIndex }),
    startCursor: convertNodeIdToCursor({ id: startIndex })
  };

  return { edges, totalCount, pageInfo };
};

export default { createPageInfo, convertCursorToNodeId, convertNodeIdToCursor };
