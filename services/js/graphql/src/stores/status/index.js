import { DB } from '../../adapters';
import { mutationAddStatus, queryByCourseIdAndLearner } from './queries';
import { convertToOffcourse, convertToGraphQL } from './converters';

const get = ({ courseId, learner }) => {
  const query = queryByCourseIdAndLearner({ courseId, learner });
  return DB.get(query).then((data) => {
    return data && convertToGraphQL(data);
  });
};

const create = ({ courseId, learner }) => {
  return {
    courseId,
    learner,
    checkpointIds: []
  };
};

const update = ({ courseId, learner, checkpointIds }, checkpointId) => {
  const tempIds = new Set(checkpointIds);

  if (tempIds.has(checkpointId)) {
    tempIds.delete(checkpointId);
  } else {
    tempIds.add(checkpointId);
  }

  const status = { courseId, learner, checkpointIds: Array.from(tempIds) };
  const query = mutationAddStatus(convertToOffcourse(status));

  return DB.put(query).then((data) => {
    return data && status;
  });
};

export default { update, create, get };
