const queryByCourseIdAndLearner = ({ courseId, learner }) => {
  return {
    TableName: process.env["statuses_table"],
    ExpressionAttributeNames: {
      "#ci": "course-id",
      "#lv": "learner"
    },
    ExpressionAttributeValues: {
      ":civ": courseId,
      ":lvv": learner
    },
    KeyConditionExpression: "#ci = :civ AND #lv = :lvv"
  };
};

const mutationAddStatus = status => {
  return {
    TableName: process.env["statuses_table"],
    Item: status
  };
};

export { mutationAddStatus, queryByCourseIdAndLearner };
