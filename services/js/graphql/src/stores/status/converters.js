const convertToGraphQL = (status) => {
  return {
    learner: status['learner'],
    courseId: status['course-id'],
    checkpointIds: status['checkpoint-ids']
  };
};

const convertToOffcourse = ({ learner, courseId, checkpointIds }) => {
  return {
    learner,
    'course-id': courseId,
    'checkpoint-ids': checkpointIds
  };
};

export { convertToOffcourse, convertToGraphQL };
