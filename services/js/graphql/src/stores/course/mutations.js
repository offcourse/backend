const mutationAddCourse = course => {
  return {
    Bucket: process.env["temp_bucket"],
    Key: `${Date.now()}.json`,
    Body: JSON.stringify(course)
  };
};

export { mutationAddCourse };
