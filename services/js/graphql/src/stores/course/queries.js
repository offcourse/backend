import { paginate } from "../../helpers";

const baseQuery = {
  index: "offcourse",
  type: "courses"
};
const defaultSort = {
  timestamp: { order: "desc" }
};

const createBody = (query, size = 10, from, sort = defaultSort) => {
  const body = { query, size };

  if (from) {
    body.from = from;
  }

  if (sort) {
    body.sort = sort;
  }

  return body;
};

const queryByCuratorAndGoal = (curator, goal) => {
  const body = createBody({
    bool: {
      must: [{ match_phrase: { goal } }, { match_phrase: { curator } }]
    }
  });
  return { ...baseQuery, body };
};

const queryByBaseId = ({
  courseId,
  baseId,
  first,
  after,
  includeSelf = true
}) => {
  // const offset = after ? paginate.convertCursorToNodeId(after) + 1 : 0;
  // return createBody({ match_phrase: { 'base-id': baseId } }, first, offset);

  const offset = after ? paginate.convertCursorToNodeId(after) + 1 : 0;
  const bool = { must: { match_phrase: { "base-id": baseId } } };

  if (!includeSelf) {
    bool.must_not = { match_phrase: { "course-id": courseId } };
  }

  const body = createBody({ bool }, first, offset);
  return { ...baseQuery, body };
};

const queryByParentId = ({
  courseId,
  parentId,
  first,
  after,
  includeSelf = false
}) => {
  const offset = after ? paginate.convertCursorToNodeId(after) + 1 : 0;

  const bool = { must: { match_phrase: { "parent-id": parentId } } };

  if (!includeSelf && courseId) {
    bool.must_not = { match_phrase: { "course-id": courseId } };
  }

  const body = createBody({ bool }, first, offset);
  return { ...baseQuery, body };
};

const queryById = id => {
  return {
    ...baseQuery,
    id
  };
};

// const queryByCollectionName = ({ collectionName, first, after }) => {
//   return {
//     Bucket: process.env["course_collections_bucket"],
//     Key: `${collectionName}.json`
//   };
// };

const queryByCollectionName = ({ collectionName, first, after }) => {
  const offset = after ? paginate.convertCursorToNodeId(after) + 1 : 0;
  const scoreSort = {
    "popularity-score": { order: "desc" }
  };
  const body = createBody({ match_all: {} }, first, offset, scoreSort);
  return { ...baseQuery, body, index: `offcourse_${collectionName}` };
};

const queryBySearchTerm = ({ searchTerm, first, after }) => {
  const offset = after ? paginate.convertCursorToNodeId(after) + 1 : 0;
  const body = createBody(
    {
      multi_match: {
        query: searchTerm,
        type: "phrase_prefix",
        fields: [
          "goal",
          "curator",
          "description",
          "checkpoints.tags",
          "checkpoints.task"
        ]
      }
    },
    first,
    offset
  );
  return { ...baseQuery, body };
};

const queryByTag = ({ tag, first, after }) => {
  const offset = after ? paginate.convertCursorToNodeId(after) + 1 : 0;
  const body = createBody(
    { bool: { should: [{ match: { "checkpoints.tags": tag } }] } },
    first,
    offset
  );
  return { ...baseQuery, body };
};

const queryByCurator = ({ curator, first, after }) => {
  const offset = after ? paginate.convertCursorToNodeId(after) + 1 : 0;
  const body = createBody(
    { bool: { should: [{ match: { curator } }] } },
    first,
    offset
  );
  return { ...baseQuery, body };
};

const queryAll = ({ first, after }) => {
  const offset = after ? paginate.convertCursorToNodeId(after) + 1 : 0;
  const body = createBody({ match_all: {} }, first, offset);
  return { ...baseQuery, body };
};

export {
  queryByBaseId,
  queryByParentId,
  queryByCollectionName,
  queryByTag,
  queryBySearchTerm,
  queryByCurator,
  queryByCuratorAndGoal,
  queryById,
  queryAll
};
