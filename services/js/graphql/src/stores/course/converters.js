import R from 'ramda';

const convertCheckpointToGraphQL = (checkpoint) => {
  return {
    task: checkpoint['task'],
    checkpointId: checkpoint['checkpoint-id'],
    resourceUrl: checkpoint['resource-url'],
    tags: checkpoint['tags']
  };
};

const converCheckpointToOffcourse = ({ task, checkpointId, resourceUrl }) => {
  return {
    task,
    'checkpoint-id': checkpointId,
    'resource-url': resourceUrl
  };
};

const convertToGraphQL = (course) => {
  return {
    goal: course['goal'],
    curator: course['curator'],
    repository: course['repository'],
    description: course['description'],
    courseId: course['course-id'],
    baseId: course['base-id'],
    parentId: course['parent-id'],
    revision: course['revision'],
    timestamp: course['timestamp'],
    checkpoint: course['checkpoint'] && convertCheckpointToGraphQL(course['checkpoint']),
    checkpoints: R.map(convertCheckpointToGraphQL, course['checkpoints'])
  };
};

const convertToOffcourse = ({
  curator,
  goal,
  revision,
  repository,
  timestamp,
  description,
  checkpoints,
  baseId,
  parentId,
  courseId
}) => {
  return {
    curator: curator.toLowerCase(),
    goal: goal.toLowerCase(),
    timestamp,
    revision,
    description,
    repository: repository.toLowerCase(),
    'base-id': baseId,
    'parent-id': parentId,
    'course-id': courseId,
    checkpoints: R.map(converCheckpointToOffcourse, checkpoints)
  };
};

export { convertToOffcourse, convertToGraphQL };
