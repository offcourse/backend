import uuid from "uuid/v4";
import R from "ramda";
import { index, dataLake } from "../../adapters";
import { paginate } from "../../helpers";
import { convertToOffcourse, convertToGraphQL } from "./converters";
import { mutationAddCourse } from "./mutations";
import {
  queryByTag,
  queryByParentId,
  queryByBaseId,
  queryByCurator,
  queryByCollectionName,
  queryByCuratorAndGoal,
  queryBySearchTerm,
  queryById,
  queryAll
} from "./queries";

const find = ({
  tag,
  courseId,
  curator,
  collectionName,
  searchTerm,
  baseId,
  parentId,
  first,
  after,
  includeSelf
}) => {
  let query = queryAll({ first, after });

  if (collectionName) {
    query = queryByCollectionName({
      collectionName,
      first,
      after
    });
    // return dataLake.get(collectionQuery).then(rawItems => {
    //   const offset = after ? paginate.convertCursorToNodeId(after) + 1 : 0;
    //   const dropTake = R.compose(
    //     R.take(first),
    //     R.drop(offset)
    //   );
    //   const items = rawItems ? dropTake(rawItems) : [];
    //   const totalCount = items.length;
    //   return paginate.createPageInfo(
    //     convertToGraphQL,
    //     items,
    //     after,
    //     totalCount
    //   );
    // });
  }

  if (curator) {
    query = queryByCurator({ curator, first, after });
  }

  if (baseId) {
    query = queryByBaseId({
      courseId,
      baseId,
      first,
      after,
      includeSelf
    });
  }

  if (parentId && !courseId) {
    query = queryByParentId({ parentId, first, after });
  }

  if (parentId && courseId) {
    query = queryByParentId({
      courseId,
      parentId,
      first,
      after,
      includeSelf
    });
  }

  if (tag) {
    query = queryByTag({ tag, first, after });
  }

  if (searchTerm) {
    query = queryBySearchTerm({ searchTerm, first, after });
  }

  return index.search(query).then(({ items, totalCount }) => {
    return paginate.createPageInfo(convertToGraphQL, items, after, totalCount);
  });
};

const findOne = ({ courseQuery }) => {
  let query = queryAll(1);
  if (!courseQuery) {
    return null;
  }

  const { curator, goal } = courseQuery;
  query = queryByCuratorAndGoal(curator, goal);

  return (
    courseQuery &&
    index.search(query).then(({ items }) => {
      const [item] = R.filter(i => i.goal === goal, items);
      return item && convertToGraphQL(item);
    })
  );
};

const get = id => {
  const query = queryById(id);
  return (
    id &&
    index.get(query).then(data => {
      return data && convertToGraphQL(data._source);
    })
  );
};

const save = graphQLCourse => {
  const offcourseCourse = convertToOffcourse(graphQLCourse);

  return dataLake.put(mutationAddCourse(offcourseCourse)).then(res => {
    return graphQLCourse;
  });
};

const update = (previousVersion, courseInput) => {
  const checkpoints = R.map(checkpoint => {
    const checkpointId = checkpoint.checkpointId || uuid();
    return { ...checkpoint, checkpointId };
  }, courseInput.checkpoints);

  return save({
    ...previousVersion,
    ...courseInput,
    checkpoints,
    timestamp: Date.now(),
    revision: previousVersion.revision + 1
  });
};

const fork = (
  { goal, courseId, baseId, checkpoints, description },
  userName
) => {
  const newCheckpoints = R.map(checkpoint => {
    return { ...checkpoint, checkpointId: uuid() };
  }, checkpoints);

  return save({
    goal,
    curator: userName,
    description,
    repository: "offcourse",
    courseId: uuid(),
    revision: 0,
    baseId,
    parentId: courseId,
    timestamp: Date.now(),
    checkpoints: newCheckpoints
  });
};

const create = ({ goal, curator, checkpoints, description }) => {
  const courseId = uuid();

  const newCheckpoints = R.map(checkpoint => {
    return { ...checkpoint, checkpointId: uuid() };
  }, checkpoints);

  return save({
    goal,
    curator,
    description,
    repository: "offcourse",
    courseId,
    revision: 0,
    baseId: courseId,
    parentId: null,
    timestamp: Date.now(),
    checkpoints: newCheckpoints
  });
};

export default {
  create,
  get,
  fork,
  update,
  find,
  findOne
};
