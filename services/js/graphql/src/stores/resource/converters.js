const convertToGraphQL = (data) => {
  return {
    title: data['title'],
    resourceUrl: data['resource-url'],
    resourceType: data['resource-type'],
    description: data['description'],
    rawContent: data['content'],
    tags: data['tags']
  };
};

export { convertToGraphQL };
