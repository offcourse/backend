import { hasher } from '../../helpers';

const queryByResourceUrl = (resourceUrl) => {
  const hash = hasher(resourceUrl);
  return {
    Bucket: process.env['resources_bucket'],
    Key: `${hash}.json`
  };
};

export { queryByResourceUrl };
