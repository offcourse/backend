import { dataLake } from '../../adapters';
import { queryByResourceUrl } from './queries';
import { convertToGraphQL } from './converters';
import { errors } from '../../helpers';

const get = (resourceUrl) => {
  const query = queryByResourceUrl(resourceUrl);
  return dataLake.get(query).then((data) => {
    return data && convertToGraphQL(data);
  });
};

export default { get };
