import courseStore from './course';
import resourceStore from './resource';
import statusStore from './status';

export { statusStore, resourceStore, courseStore };
