"use strict";

const redirectResponse = {
  status: "302",
  statusDescription: "Found",
  headers: {
    location: [
      {
        key: "Location",
        value: "/introduction"
      }
    ]
  }
};

exports.handler = (event, context, callback) => {
  const { request } = event.Records[0].cf;
  const { headers, uri } = request;
  const { cookie } = headers;
  const visitsHomepage = uri == "/";

  if (!visitsHomepage) {
    return callback(null, request);
  }

  if (!cookie) {
    return callback(null, redirectResponse);
  }
  console.log(cookie);

  // const hasBeenHere = cookie
  //   .reduce((a, c) => [...a, ...c.keys()], [])
  //   .includes("firstVisit");

  // if (hasBeenHere) {
  //   callback(null, request);
  // }

  return callback(null, request);
};
