import { searchCluster } from "./config";
import { map, filter, chain, identity } from "ramda";

const remove = ({ indexName }) => {
  return searchCluster.indices
    .delete({
      index: indexName
    })
    .catch(err => console.log(err));
};

const getIndexQuery = chain(item => {
  const index = {
    _index: "offcourse_popular",
    _type: "courses",
    _id: item["course-id"]
  };
  return [{ index }, item];
});

const saveAll = ({ indexName, items }) => {
  const body = getIndexQuery(items);
  return searchCluster.bulk({ body });
};

const coursesByIdsQuery = ids => {
  return {
    index: "offcourse",
    type: "courses",
    body: {
      ids
    }
  };
};

const search = async ({ ids }) => {
  const query = coursesByIdsQuery(ids);
  const { docs } = await searchCluster.mget(query);
  const rawItems = map(item => item._source, docs);
  return filter(identity, rawItems);
};

export { remove, saveAll, search };
