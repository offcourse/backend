import { bigTable } from "./config";

const execute = query => {
  return new Promise((resolve, reject) => {
    bigTable.execute(query, (error, res) => {
      error ? reject(error) : resolve(res.records);
    });
  });
};

export { execute };
