import { map, reduce, curry, keys } from "ramda";
import { tableName } from "./config";
import { popularCoursesQuery } from "./queries";
import * as bigTable from "./bigTable";
import * as es from "./es";

const getCourseData = reduce((acc, row) => {
  return { [row.courseid]: row.score, ...acc };
}, {});

const _addScore = (courseData, item) => {
  const courseId = item["course-id"];
  const score = parseInt(courseData[courseId]);
  return {
    "popularity-score": score,
    ...item
  };
};

exports.handler = async (_, __, callback) => {
  try {
    const query = popularCoursesQuery({ tableName });
    const records = await bigTable.execute(query);
    const courseData = getCourseData(records);

    const docs = await es.search({ ids: keys(courseData) });

    const addScore = curry(_addScore)(courseData);
    const items = map(addScore, docs);
    const indexName = "offcourse_popular";
    await es.remove({ indexName });
    const res = await es.saveAll({ indexName, items });

    callback(null, { res });
  } catch (err) {
    callback(err, null);
  }
};
