const popularCoursesQuery = ({ tableName, batchSize = 100 }) => `
SELECT object.curator,
         object.goal,
         object.courseId,
         sum(CASE verb
    WHEN 'VIEWED' THEN
    1
    WHEN 'UPDATED'THEN 2
    WHEN 'COMPLETED' THEN
    3
    WHEN 'FORKED'THEN 10
    ELSE 0 END) AS score
FROM ${tableName}
WHERE (object.type = 'COURSE'
        OR object.type = 'CHECKPOINT')
GROUP BY  object.curator, object.goal, object.courseId
ORDER BY  score DESC
LIMIT ${batchSize}
`;

export { popularCoursesQuery };
