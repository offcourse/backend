import Mercury from "@postlight/mercury-parser";
import Zip from "node-zip";

const handler = async (event, __, callback) => {
  try {
    console.log(event);
    const url = event["resource-url"];
    const result = await Mercury.parse(url);
    const zipper = new Zip();
    const resourceJSON = JSON.stringify(result);
    const resource = zipper
      .file("data", resourceJSON)
      .generate({ base64: true, compression: "DEFLATE" });
    callback(null, { ...event, "raw-resource": resource });
  } catch (err) {
    callback(err, null);
  }
};

export { handler };
