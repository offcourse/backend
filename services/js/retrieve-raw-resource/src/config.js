import HttpAwsEs from "http-aws-es";
import AWS from "aws-sdk";
const dbName = "offcourse-database";
const tableName = "offcourse_stream_events";

AWS.config.update({
  region: "us-east-1"
});

const searchCluster = require("elasticsearch").Client({
  hosts: [process.env["search_cluster"]],
  connectionClass: HttpAwsEs
});

const clientConfig = {
  bucketUri: `s3://${process.env["big_table_artifacts"]}`,
  database: dbName
};

const awsConfig = {
  region: "us-east-1"
};

const athena = require("athena-client");

const bigTable = athena.createClient(clientConfig, awsConfig);

export { bigTable, dbName, tableName, searchCluster };
