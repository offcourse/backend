(defproject offcourse/services "0.1.0-SNAPSHOT"
  :description     "This is an echo lambda function. It's mainly used for debugging purposes."
  :url             "https://gitlab.com/offcourse/services"
  :license         {:name "MIT License"
                    :url "http://opensource.org/licenses/MIT"}
  :aliases         {"watch"         ["do" "build" ["cljsbuild" "auto"]]
                    "build"         ["do" "clean" ["npm" "install"] ["cljsbuild" "once"]]
                    "build-zip"     ["do" "build" "zip"]
                    "invoke"        ["shell" "node-lambda" "run" "-M" "false" "-H" "index.handler" "-j"]}
  :dependencies    [[org.clojure/clojure         "1.9.0-alpha17"]
                    [org.clojure/core.async      "0.3.443"]
                    [org.clojure/clojurescript   "1.9.946"]
                    [offcourse/specs             "0.1.24"]
                    [offcourse/shared            "0.11.27"]
                    [offcourse/backend-shared    "0.5.62"]]
  :plugins         [[lein-cljsbuild "1.1.7"]
                    [lein-shell "0.5.0"]
                    [lein-zip "0.1.1"]
                    [lein-npm "0.7.0-rc1"]])
