(ns offcourse.distribute.core
  (:require [backend-shared.service.index :as service]
            [offcourse.distribute.perform]
            [offcourse.distribute.fetch]
            [offcourse.distribute.config :refer [config]]
            [shared.protocols.actionable :as ac]
            [shared.protocols.eventful :as ev])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defn ^:export handler [& args]
  (.log js/console (.. js/process -env -handle_bucket_event_flow))
  (go
    (let [{:keys [event] :as service}  (service/create config args)
          res    (<! (ac/perform service [:trigger event]))]
      (ev/respond service res))))

(defn -main [] identity)
(set! *main-cli-fn* -main)
