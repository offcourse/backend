(ns offcourse.distribute.perform
  (:require [backend-shared.service.index :refer [perform]]
            [shared.protocols.loggable :as log]
            [backend-shared.converters.index :refer
             [db-event->event bucket-event->bucket-query]]
            [cljs.core.async :as async :refer [<!]]
            [shared.protocols.actionable :as ac]
            [shared.protocols.queryable :as qa]
            [shared.models.error.index :as error]
            [shared.models.event.index :as event])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defmethod perform [:trigger :bucket-event] [service [_ payload :as action]]
  (go
    (let [query (bucket-event->bucket-query payload)
          [status payload :as res] (<! (qa/fetch service query))]
      (if (= status :found)
        (<! (ac/perform service [:trigger payload]))
        (event/create [:failed (error/create :unknown-key payload)])))))

(defmethod perform [:trigger :course] [{:keys [state-machine]} action]
  (ac/perform state-machine action))

(defmethod perform [:trigger :status] [{:keys [state-machine]} action]
  (ac/perform state-machine action))

(defmethod perform [:trigger :courses] [{:keys [state-machine]} action]
  (ac/perform state-machine action))

(defmethod perform [:trigger :course-queries] [{:keys [state-machine]} action]
  (ac/perform state-machine action))

(defmethod perform [:trigger :repo-item] [{:keys [state-machine]} action]
  (ac/perform state-machine action))

(defmethod perform [:trigger :incomplete-bookmark] [{:keys [state-machine]} action]
  (ac/perform state-machine action))

(defmethod perform [:trigger :bookmark] [{:keys [state-machine]} action]
  (ac/perform state-machine action))

(defmethod perform [:trigger :db-event] [this [_ payload :as action]]
  (go
    (let [[_ {:keys [removed saved] :as r}] (db-event->event payload)]
      (cond
        saved (<! (ac/perform this [:trigger saved]))
        removed (let [queries (mapv #(select-keys %1 [:course-id :revision]) removed)]
                  (<! (ac/perform this [:trigger queries])))))))
