(ns offcourse.distribute.config
    (:require [cljs.nodejs :as node]
              [offcourse.distribute.specs :refer [specs]]
              [shared.protocols.convertible :as cv]))

(def adapters {:bucket
               {:bucket-names {}}
               :state-machine
               {:machine-names
                {:repo-item           (.. js/process -env -import_course_flow)
                 :status              (.. js/process -env -save_status_flow)
                 :statuses            (.. js/process -env -save_statuses_flow)
                 :incomplete-bookmark (.. js/process -env -missing_bookmark_flow)
                 :course              (.. js/process -env -process_course_flow)
                 :courses             (.. js/process -env -index_courses_flow)
                 :course-queries      (.. js/process -env -remove_courses_flow)
                 :bookmark            (.. js/process -env -process_bookmark_flow)}}})

(def config {:name :distribute
             :adapters adapters
             :specs specs})
