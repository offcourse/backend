(ns offcourse.distribute.specs
  (:require [cljs.spec.alpha :as spec]))

(defmulti event-spec (fn [[event-type _ :as event]] event-type))
(defmulti action-spec (fn [[action-type _ :as action]] action-type))

(spec/def :distribute/query   (spec/or :bucket-item   :aws/bucket-item))

(spec/def ::execution-arn string?)
(spec/def ::start-date string?)
(spec/def ::execution-data (spec/keys :req-un [::execution-arn ::start-date]))

(defmethod event-spec :triggered [_]
  (spec/tuple :event/types (spec/or :execution-data ::execution-data)))

(defmethod event-spec :failed [_]
  (spec/tuple :event/types (spec/or :error :error/error)))

(defmethod event-spec :removed [_]
  (spec/tuple :event/types (spec/or :courses (spec/coll-of :course/valid)
                                    :statuses (spec/coll-of :status/valid))))

(defmethod action-spec :trigger [_]
  (spec/tuple :action/types (spec/or :bucket-event        :aws/bucket-event
                                     :db-event            :aws/db-event
                                     :course              :course/valid
                                     :courses             (spec/coll-of :course/valid)
                                     :course-queries      (spec/coll-of :course/query)
                                     :bookmark            :bookmark/valid
                                     :incomplete-bookmark :bookmark/augmented
                                     :status              :status/valid
                                     :repo-item           :github/item-query
                                     :error               :error/error)))

(def specs {:event (spec/multi-spec event-spec :event-type)
            :query :distribute/query
            :action (spec/multi-spec action-spec :action-type)})
