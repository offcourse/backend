(ns offcourse.distribute.fetch
  (:require [backend-shared.service.index :refer [fetch]]
            [shared.protocols.queryable :as qa])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defmethod fetch :bucket-item [{:keys [bucket]} query]
  (qa/fetch bucket query))
