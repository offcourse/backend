(ns offcourse.transform.core
  (:require [backend-shared.service.index :as service]
            [cljs.core.async :refer [<!]]
            [offcourse.transform.perform]
            [shared.protocols.actionable :as ac]
            [shared.protocols.eventful :as ev]
            [offcourse.transform.config :refer [config]])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defn ^:export handler [& args]
  (go
    (let [{:keys [event] :as service} (service/create config args)
          converted                    (<! (ac/perform service [:transform event]))]
      (ev/respond service converted))))

(defn -main [] identity)
(set! *main-cli-fn* -main)
