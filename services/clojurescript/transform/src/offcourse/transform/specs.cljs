(ns offcourse.transform.specs
  (:require [cljs.spec.alpha :as spec]))

(defmulti action-spec (fn [[action-type _ :as action]] action-type))
(defmulti event-spec (fn [[event-type _ :as event]] event-type))

(defmethod action-spec :transform [_]
  (spec/tuple :action/types (spec/or      :compressed-bookmark :bookmark/compressed-resource
                                          :compressed-data     :offcourse/compressed
                                          :mercury-resource    :mercury/resource
                                          :augmented-bookmark  :bookmark/augmented
                                          :db-event            :aws/db-event
                                          :bucket-event        :aws/bucket-event
                                          :course              :course/valid
                                          :repo                :github/repo-query
                                          :repo-item           :github/item-query
                                          :error               :error/error)))

(defmethod action-spec :deflate [_]
  (spec/tuple :action/types (spec/or :compressed-data     :offcourse/compressed)))

(defmethod action-spec :compress [_]
  (spec/tuple :action/types (spec/or :resource  :resource/valid
                                     :error     :error/error)))

(defmethod event-spec :transformed [_]
  (spec/tuple :event/types (spec/or :repo-items        (spec/coll-of :github/item-query)
                                    :course-update     :course/update
                                    :course            :course/unchecked
                                    :compressed-data   :offcourse/compressed
                                    :bucket-query      :bucket/query
                                    :resource          :resource/valid
                                    :courses           (spec/coll-of :course/valid)
                                    :bookmarks         (spec/coll-of :bookmark/valid))))

(defmethod event-spec :failed [_]
  (spec/tuple :event/types (spec/or :error :error/error)))

(def specs {:event (spec/multi-spec event-spec :event-type)
            :action (spec/multi-spec action-spec :action-type)})
