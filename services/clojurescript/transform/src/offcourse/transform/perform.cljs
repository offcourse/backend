(ns offcourse.transform.perform
  (:require [backend-shared.converters.index
             :refer
             [mercury->resource course->bookmarks course->update db-event->event
              raw->user-data repo->items repo-item->course]]
            [backend-shared.service.index :refer [perform]]
            [cljs.core.async :refer [<!]]
            [shared.models.course.index :as course]
            [shared.models.error.index :as error]
            [shared.models.event.index :as event]
            [shared.protocols.actionable :as ac]
            [shared.protocols.loggable :as log])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defmethod perform [:transform :compressed-data] [service [action-type data]]
  (go
    (let [[_ raw-resource]    (<! (ac/perform service [:deflate data]))
          [_ resource]        (<! (ac/perform service [:transform raw-resource]))
          [_ compressed-data] (<! (ac/perform service [:compress resource]))]
      (event/create [:transformed compressed-data]))))

(defmethod perform [:transform :compressed-bookmark] [service [action-type bookmark-data]]
  (go
    (let [[_ resource] (<! (ac/perform service [:deflate (:resource bookmark-data)]))
          payload      (assoc bookmark-data :resource resource)
          [_ bookmark] (<! (ac/perform service [:transform payload]))]
      (event/create [:transformed bookmark]))))

(defmethod perform [:transform :mercury-resource] [_ [_ payload]]
  (go
    (event/create [:transformed (mercury->resource payload)])))

(defmethod perform [:transform :augmented-bookmark] [_ [_ payload]]
  (go
    (event/create [:transformed (course->update payload)])))

(defmethod perform [:transform :repo-item] [_ [_ payload]]
  (go
    (event/create [:transformed (repo-item->course payload)])))

(defmethod perform [:transform :course] [_ [_ payload]]
  (go
    (event/create [:transformed (course->bookmarks payload)])))

(defmethod perform [:transform :repo] [_ [_ payload]]
  (go
    (event/create [:transformed (repo->items payload)])))

(defmethod perform :default [_ [_ payload]]
  (go
    (event/create [:failed (error/create :unsupported-payload payload)])))
