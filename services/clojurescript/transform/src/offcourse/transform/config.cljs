(ns offcourse.transform.config
    (:require [cljs.nodejs :as node]
              [offcourse.transform.specs :refer [specs]]
              [shared.protocols.convertible :as cv]))

(def config {:name :transform
             :adapters {}
             :specs specs})
