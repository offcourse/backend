(ns offcourse.echo.perform
    (:require [backend-shared.service.index :refer [perform]]
               [cljs.core.async :as async :refer [<!]]
               [shared.protocols.actionable :as ac])
    (:require-macros [cljs.core.async.macros :refer [go]]))

;; (defmethod perform [:sign-up :raw-user] [{:keys [bucket]} [_ user :as action]]
;;   (ac/perform bucket [:put [user]]))
