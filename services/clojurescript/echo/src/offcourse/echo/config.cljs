(ns offcourse.echo.config
    (:require [cljs.nodejs :as node]
              [offcourse.echo.specs :refer [specs]]
              [shared.protocols.convertible :as cv]))

(def config {:name :echo
             :adapters {}
             :specs specs})
