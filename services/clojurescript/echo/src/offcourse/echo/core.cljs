(ns offcourse.echo.core
  (:require [backend-shared.service.index :as service]
            [offcourse.echo.config :refer [config]]
            [shared.models.event.index :as event]
            [shared.protocols.eventful :as ev]
            [shared.protocols.loggable :as log]
            [shared.protocols.eventful :as ev])
  (:require-macros [cljs.core.async.macros :refer [go]]))


(defn ^:export handler [& args]
  (go
    (let [service  (service/create config args)
          event    (event/create [:received (:event service)])]
      (ev/respond service event))))

(defn -main [] identity)
(set! *main-cli-fn* -main)
