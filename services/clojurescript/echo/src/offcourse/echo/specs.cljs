(ns offcourse.echo.specs
  (:require [cljs.spec.alpha :as spec]))

(defmulti event-spec (fn [[event-type _ :as event]] event-type))

(defmethod event-spec :received [_]
  (spec/tuple :event/types (spec/or :any any?)))

(def specs {:event (spec/multi-spec event-spec :event-type)})
