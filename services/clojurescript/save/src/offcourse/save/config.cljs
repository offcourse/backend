(ns offcourse.save.config
    (:require [cljs.nodejs :as node]
              [offcourse.save.specs :refer [specs]]
              [shared.protocols.convertible :as cv]))

(def adapters
  {:bucket
   {:bucket-names
    {:portrait         (.. js/process -env -assets_bucket)
     :course           (.. js/process -env -temp_bucket)
     :raw-resource     (.. js/process -env -raw_resources_bucket)
     :repo-items       (.. js/process -env -temp_bucket)
     :resource         (.. js/process -env -resources_bucket)
     :missing-resource (.. js/process -env -temp_bucket)
     :statuses         (.. js/process -env -statuses_bucket)
     :bookmarks        (.. js/process -env -bookmarks_bucket)}}
   :index
   {:search-url        (.. js/process -env -search-cluster)}
   :db
   {:table-names
    {:status           (.. js/process -env -statuses_table)
     :course           (.. js/process -env -courses_table)}}})

(def config {:name :save
             :adapters adapters
             :specs specs})
