(ns offcourse.save.specs
  (:require [cljs.spec.alpha :as spec]))

(defmulti action-spec (fn [[action-type _ :as action]] action-type))
(defmulti event-spec (fn [[event-type _ :as event]] event-type))

(spec/def ::payload (spec/or :unchecked-course :course/unchecked
                             :status             :status/valid
                             :course             :course/valid
                             :course-update      :course/update
                             :resource           :resource/valid
                             :portrait           :portrait/valid
                             :compressed-data    :offcourse/compressed
                             :missing-resource   :bookmark/missing-resource
                             :raw-resource       :resource/raw
                             :statuses           (spec/coll-of :status/valid)
                             :courses            (spec/coll-of :course/valid)
                             :bookmarks          (spec/coll-of :bookmark/valid)
                             :repo-items         (spec/coll-of :github/item-query)))

(defmethod action-spec :deflate [_]
  (spec/tuple :action/types (spec/or :compressed-data :offcourse/compressed)))

(defmethod action-spec :save [_]
  (spec/tuple :action/types ::payload))

(defmethod event-spec :saved [_]
  (spec/tuple :event/types ::payload))

(defmethod event-spec :failed [_]
  (spec/tuple :event/types (spec/or :error :error/error)))

(def specs {:event (spec/multi-spec event-spec :event-type)
            :action (spec/multi-spec action-spec :action-type)})
