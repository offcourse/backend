(ns offcourse.save.perform
  (:require [backend-shared.service.index :refer [perform]]
            [shared.models.error.index :as error]
            [shared.protocols.loggable :as log]
            [shared.models.event.index :as event]
            [shared.protocols.actionable :as ac])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defmethod perform [:save :unchecked-course] [{:keys [bucket]} action]
  (ac/perform bucket action))

(defmethod perform [:save :course] [{:keys [db]} action]
  (ac/perform db action))

(defmethod perform [:save :status] [{:keys [db]} action]
  (ac/perform db action))

(defmethod perform [:save :statuses] [{:keys [bucket]} action]
  (ac/perform bucket action))

(defmethod perform [:save :course-update] [{:keys [db]} [_ payload]]
  (ac/perform db [:update payload]))

(defmethod perform [:save :courses] [{:keys [index]} action]
  (ac/perform index action))

(defmethod perform [:save :portrait] [{:keys [bucket]} action]
  (ac/perform bucket action))

(defmethod perform [:save :bookmarks] [{:keys [bucket]} action]
  (ac/perform bucket action))

(defmethod perform [:save :missing-resource] [{:keys [bucket]} action]
  (ac/perform bucket action))

(defmethod perform [:save :repo-items] [{:keys [bucket]} action]
  (ac/perform bucket action))

(defmethod perform [:save :resource] [{:keys [bucket]} action]
  (ac/perform bucket action))

(defmethod perform [:save :raw-resource] [{:keys [bucket]} action]
  (ac/perform bucket action))

(defmethod perform [:save :compressed-data] [{:keys [bucket] :as service} [action-type payload :as action]]
  (go
    (let [[_ data :as res] (<! (ac/perform service [:deflate payload]))]
      (<! (ac/perform service [action-type data])))))
