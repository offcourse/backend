(ns offcourse.save.core
  (:require [backend-shared.service.index :as service]
            [offcourse.save.perform]
            [cljs.core.async :refer [<!]]
            [offcourse.save.config :refer [config]]
            [shared.protocols.actionable :as ac]
            [shared.protocols.eventful :as ev])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defn ^:export handler [& args]
  (go
    (let [{:keys [event] :as service} (service/create config args)
          res (<! (ac/perform service [:save event]))]
      (ev/respond service res))))

(defn -main [] identity)
(set! *main-cli-fn* -main)
