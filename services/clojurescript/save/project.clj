(defproject offcourse/save "0.1.0-SNAPSHOT"
  :description     "This is an save lambda function. It's mainly used for debugging purposes."
  :plugins         [[lein-parent "0.3.2"]]
  :parent-project {:path "../project.clj"
                   :inherit [:aliases :plugins :dependencies :url :license]}
  :zip             ["node_modules" "index.js" "target/main.js"]
  :template-additions ["context.json" "index.js" "event.json"]
  :cljsbuild       {:builds [{:id "prod"
                              :source-paths ["src"]
                              :compiler {:main offcourse.save.core
                                         :output-to "target/main.js"
                                         :target :nodejs
                                         :install-deps true
                                         :optimizations :simple
                                         :pretty-print true
                                         :parallel-build true}}]})
