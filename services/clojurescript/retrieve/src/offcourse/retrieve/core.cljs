(ns offcourse.retrieve.core
  (:require [backend-shared.service.index :as service]
            [offcourse.retrieve.config :refer [config]]
            [offcourse.retrieve.fetch]
            [cljs.core.async :refer [<!]]
            [shared.protocols.convertible :as cv]
            [shared.protocols.eventful :as ev]
            [shared.protocols.queryable :as qa])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defn ^:export handler [& args]
  (go
    (let [{:keys [event] :as service} (service/create config args)
          res    (<! (qa/fetch service event))]
      (ev/respond service res))))

(defn -main [] identity)
(set! *main-cli-fn* -main)
