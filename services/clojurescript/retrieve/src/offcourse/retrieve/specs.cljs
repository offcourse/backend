(ns offcourse.retrieve.specs
  (:require [cljs.spec.alpha :as spec]))

(defmulti action-spec (fn [[action-type _ :as action]] action-type))
(defmulti event-spec (fn [[event-type _ :as event]] event-type))

(spec/def :retrieve/query (spec/or :course                :course/query
                                    :raw-resource         :query/missing-raw-resource
                                    :cached-raw-resource  :query/missing-resource
                                    :resource             :resource/query
                                    :repo                 :github/repo-query
                                    :repo-item            :github/item-query
                                    :portrait             :portrait/valid))

(defmethod action-spec :compress [_]
  (spec/tuple :action/types (spec/or :raw-resource :resource/raw
                                     :resource     :resource/valid
                                     :error        :error/error)))

(defmethod event-spec :found [_]
  (spec/tuple :event/types (spec/or :course       :course/valid
                                    :raw-resource :resource/raw
                                    :resource     :resource/valid
                                    :portrait     string?
                                    :repo-item    :github/repo-item
                                    :repo         :github/repo
                                    :raw-course   :course/raw
                                    :error        :error/error)))

(defmethod event-spec :compressed [_]
  (spec/tuple :event/types (spec/or :compressed-data :offcourse/compressed)))

(defmethod event-spec :not-found [_]
  (spec/tuple :event/types (spec/or :not-found #{:not-found})))

(defmethod event-spec :failed [_]
  (spec/tuple :event/types (spec/or :error :error/error)))

(def specs {:event (spec/multi-spec event-spec :event-type)
            :action (spec/multi-spec action-spec :action-type)
            :query  :retrieve/query})
