(ns offcourse.retrieve.fetch
  (:require [backend-shared.service.index :refer [fetch]]
            [shared.models.event.index :as event]
            [cljs.core.async :refer [<!]]
            [shared.protocols.actionable :as ac]
            [shared.protocols.queryable :as qa]
            [shared.protocols.loggable :as log])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defmethod fetch :course [{:keys [db]} query]
  (go
    (let [[status payload :as res] (<! (qa/fetch db query))]
      (case status
        :found     (event/create [:found payload])
        :not-found (event/create [:not-found :not-found])
       res))))

(defmethod fetch :raw-resource [{:keys [http] :as service} query]
  (go
    (let [[status payload :as res] (<! (qa/fetch http query))]
      (case status
        :found     (<! (ac/perform service [:compress payload]))
        :not-found (event/create [:not-found :not-found])
        res))))

(defmethod fetch :cached-raw-resource [{:keys [bucket] :as service} query]
  (go
    (let [[status payload :as res] (<! (qa/fetch bucket query))]
      (case status
        :found     (<! (ac/perform service [:compress payload]))
        :not-found (event/create [:not-found :not-found])
        res))))

(defmethod fetch :resource [{:keys [bucket] :as service} query]
  (go
    (let [[status payload :as res] (<! (qa/fetch bucket (select-keys query [:resource-url])))]
      (case status
        :found     (<! (ac/perform service [:compress payload]))
        :not-found (event/create [:not-found :not-found])
        res))))

(defmethod fetch :default [{:keys [http]} query]
  (go
    (let [[status payload :as res] (<! (qa/fetch http query))]
      (case status
        :found     (event/create [:found payload])
        :not-found (event/create [:not-found :not-found])
        res))))
