(ns offcourse.retrieve.perform
  (:require [backend-shared.service.index :refer [perform]]
            [cljs.core.async :refer [<!]]
            [shared.protocols.queryable :as qa]
            [shared.protocols.loggable :as log])
  (:require-macros [cljs.core.async.macros :refer [go]]))
