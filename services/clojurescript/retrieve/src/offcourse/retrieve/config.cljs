(ns offcourse.retrieve.config
    (:require [cljs.nodejs :as node]
              [offcourse.retrieve.specs :refer [specs]]
              [shared.protocols.convertible :as cv]))

(def adapters {:http   {:api-keys    {:mercury [(.. js/process -env -mercury_api_key)]
                                      :github [(.. js/process -env -github_api_key)]}}
               :db     {:table-names {:course (.. js/process -env -courses_table)}}
               :bucket {:bucket-names {:resource (.. js/process -env -resources_bucket)
                                       :raw-resource (.. js/process -env -raw_resources_bucket)}}})

(def config {:name :retrieve
             :adapters adapters
             :specs specs})
