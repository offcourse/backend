(ns offcourse.remove.config
    (:require [cljs.nodejs :as node]
              [offcourse.remove.specs :refer [specs]]
              [shared.protocols.convertible :as cv]))

(def adapters
  {:index
   {:search-url           (.. js/process -env -search-cluster)}})

(def config {:name :remove
             :adapters adapters
             :specs specs})
