(ns offcourse.remove.perform
  (:require [backend-shared.service.index :refer [perform]]
            [shared.models.error.index :as error]
            [shared.protocols.loggable :as log]
            [shared.models.event.index :as event]
            [shared.protocols.actionable :as ac])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defmethod perform [:remove :course-queries] [{:keys [index]} action]
  (ac/perform index action))

