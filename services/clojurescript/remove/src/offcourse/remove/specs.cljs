(ns offcourse.remove.specs
  (:require [cljs.spec.alpha :as spec]))

(defmulti action-spec (fn [[action-type _ :as action]] action-type))
(defmulti event-spec (fn [[event-type _ :as event]] event-type))

(spec/def ::payload (spec/or :course-queries (spec/coll-of :course/query)))

(defmethod action-spec :remove [_]
  (spec/tuple :action/types ::payload))

(defmethod event-spec :removed [_]
  (spec/tuple :event/types ::payload))

(defmethod event-spec :failed [_]
  (spec/tuple :event/types (spec/or :error :error/error)))

(def specs {:event (spec/multi-spec event-spec :event-type)
            :action (spec/multi-spec action-spec :action-type)})
