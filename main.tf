variable aws_access_key {}
variable aws_secret_key {}
variable aws_region {}
variable application {}
variable zone_id {}
variable domain_name {}
variable certificate_arn {}
variable github_api_key {}
variable mercury_api_key {}
variable algolia_application_id {}
variable algolia_admin_key {}

data "aws_caller_identity" "current" {}

terraform {
  backend "s3" {
    bucket         = "terraform-state-offcourse"
    key            = "platform/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform_locks"
  }
}

provider template {
  version = "~> 0.1"
}

provider aws {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"

  version = "1.50.0"
}

data "aws_acm_certificate" "domain" {
  domain   = "www.offcourse.io"
  statuses = ["ISSUED"]
}

module stack {
  source      = "./modules/stack"
  application = "${var.application}"
  region      = "${var.aws_region}"

  zone = {
    domain_name     = "${var.domain_name}"
    zone_id         = "${var.zone_id}"
    certificate_arn = "${data.aws_acm_certificate.domain.arn}"
  }

  api_keys = {
    github                 = "${var.github_api_key}"
    mercury                = "${var.mercury_api_key}"
    algolia_application_id = "${var.algolia_application_id}"
    algolia_admin_key      = "${var.algolia_admin_key}"
  }
}
