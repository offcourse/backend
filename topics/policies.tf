resource "aws_iam_policy" "publish" {
  name   = "${var.application}-failed_events_topic"
  policy = "${data.aws_iam_policy_document.publish.json}"
}
