data "aws_iam_policy_document" "publish" {
  statement {
    actions = [
      "sns:Publish"
    ]
    resources = [
      "${aws_sns_topic.failed_events.arn}"
    ]
  }
}
