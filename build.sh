!/bin/bash

mkdir -p artifacts/functions artifacts/flows

for dir in "retrieve-raw-resource" "graphql" "authorize" "redirect" "gather-popular-courses" 
do
    echo $dir
    cd "../backend/services/js/$dir/"
    npm run release
    _ERR=$?
    if [ $_ERR -gt 0 ] ; then
        echo "Build Failed" >&2
        exit 255
    fi
    cp ./*.zip ../../../artifacts/functions
    rm $dir.zip
    cd $OLDPWD
done

for dir in "distribute" "save" "transform" "echo" "retrieve" "remove";
do
  echo $dir
  cd "../backend/services/clojurescript/$dir"
  lein build-zip
  _ERR=$?
  if [ $_ERR -gt 0 ] ; then
    echo "Build Failed" >&2
    exit 255
  fi
  cp ./target/*.zip ../../../artifacts/functions
  cd $OLDPWD
done


cd flows
sh convert-flows.sh
cp ./target/*.json.tpl ../artifacts/flows
cd $OLDPWD
