resource "aws_iam_role" "redirect" {
  name = "${var.application}-redirect_edge_function"
  assume_role_policy = "${var.policy_documents["assume_role"]}"
}
resource "aws_iam_role_policy" "monitoring" {
  name = "${var.application}-monitoring_policy"
  role = "${aws_iam_role.redirect.id}"
  policy = "${var.policy_documents["monitoring"]}"
}

