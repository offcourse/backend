output info {
  value = {
    arn = "${aws_lambda_function.redirect.arn}"
    qualified_arn = "${aws_lambda_function.redirect.qualified_arn}"
  }
}
