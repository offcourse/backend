resource "aws_lambda_alias" "redirect_production" {
  name             = "production"
  description      = "the alias for our production env"
  function_name    = "${aws_lambda_function.redirect.arn}"
  function_version = "107"
} 