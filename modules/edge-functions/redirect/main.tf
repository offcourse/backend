resource "aws_lambda_function" "redirect" {
  function_name    = "${var.application}-redirect-edge"
  handler          = "index.handler"
  runtime          = "nodejs6.10"
  filename         = "${var.file_name}"
  source_code_hash = "${base64sha256(file("${var.file_name}"))}"
  role             = "${aws_iam_role.redirect.arn}"
  memory_size      = 128
  timeout          = 5
  publish          = true
}
