module redirect {
  source      = "./redirect"
  application = "${var.application}"
  file_name   = "${var.artifacts_dir}/redirect.zip"

  policy_documents = {
    assume_role = "${data.aws_iam_policy_document.assume_role.json}"
    monitoring  = "${data.aws_iam_policy_document.monitoring.json}"
  }
}
