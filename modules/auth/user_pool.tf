resource "aws_cognito_user_pool" "pool" {
  name                       = "${var.application}-user-pool"
  auto_verified_attributes   = ["email"]
  alias_attributes           = ["email"]
  sms_verification_message   = "Your registration code is {####}"
  sms_authentication_message = "Your registration code is {####}"

  verification_message_template {
    default_email_option = "CONFIRM_WITH_CODE"
    email_subject        = "Offcourse Verification Code"
    email_message        = "<p>Thanks for signing up!</p><p><strong>Your registration code is {####}</strong></p><p>You are now registered on the Offcourse platform. Start your learning process and share what you know with others in the Offcourse community.</p><p>One thing to keep in mind: our platform is currently only supported in Chrome for Desktop, so make sure to choose Chrome when using the platform. Also, please be aware that we are in beta stage of development. This means we will continuously improve and update the platform.</p></p>We are happy to receive feedback you have on the platform that can help us improve. If you have questions, please consult our <a href=\"https://gitlab.com/offcourse/support/blob/master/FAQ.md\">FAQ</a> to see if your question is listed there. If not, you can ask for support via <a href=\"https://goo.gl/forms/rcCDVkMi0HC6Ezvi2\">this link</a>.</p><p>If you would like to contribute in order to improve the platform, please shoot us an email at contact@offcourse.io. We have collaboration opportunities for developers, designers and bloggers.</p><p>Let’s develop together,</p><p><em>Team Offcourse</em></p>"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes  = ["schema"]
  }

  email_configuration {
    reply_to_email_address = "contact@${var.domain_name}"
  }

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = false
    name                     = "email"
    required                 = true
  }

  password_policy {
    minimum_length    = 8
    require_lowercase = true
    require_numbers   = true
    require_symbols   = true
    require_uppercase = true
  }

  tags {
    Application = "${var.application}"
  }
}
