output user_pool {
  value = {
    id            = "${aws_cognito_user_pool.pool.id}"
    arn           = "arn:aws:cognito-idp:${var.region}:${var.identity}:userpool/${aws_cognito_user_pool.pool.id}"
    iss           = "https://cognito-idp.${var.region}.amazonaws.com/${aws_cognito_user_pool.pool.id}"
  }
}
