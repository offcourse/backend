data "template_file" "import_repo_flow" {
  template =  "${file("${var.templates_dir}/import-repo-flow.json.tpl")}"

  vars {
    save      = "${lookup(var.services["save"], "arn")}"
    transform = "${lookup(var.services["transform"], "arn")}"
    retrieve  = "${lookup(var.services["retrieve"], "arn")}"
  }
}

resource "aws_sfn_state_machine" "import_repo_flow" {
  name       = "${var.application}-import_repo"
  role_arn   = "${aws_iam_role.flows.arn}"
  definition = "${data.template_file.import_repo_flow.rendered}"
}

module import_repo_flow_policies {
  source      = "./policies"
  application = "${var.application}"
  flow_name   = "import_repo"
  flow_arn    = "${aws_sfn_state_machine.import_repo_flow.id}"
}

output "import_repo" {
  value = {
    arn = "${aws_sfn_state_machine.import_repo_flow.id}"
    trigger_policy = "${module.import_repo_flow_policies.info["trigger"]}"
  }
}
