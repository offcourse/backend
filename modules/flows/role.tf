resource "aws_iam_role" "flows" {
  name = "${var.application}-flows"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role.json}"
}

resource "aws_iam_role_policy" "flows" {
  name = "${var.application}-flows"
  role = "${aws_iam_role.flows.id}"
  policy = "${data.aws_iam_policy_document.flow.json}"
}
