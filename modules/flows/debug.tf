data "template_file" "debug_flow" {
  template =  "${file("${var.templates_dir}/debug.json.tpl")}"

  vars {
    echo = "${lookup(var.services["echo"], "arn")}"
  }
}

resource "aws_sfn_state_machine" "debug_flow" {
  name       = "${var.application}-debug"
  role_arn   = "${aws_iam_role.flows.arn}"
  definition = "${data.template_file.debug_flow.rendered}"
}

module debug_flow_policies {
  source      = "./policies"
  application = "${var.application}"
  flow_name   = "debug"
  flow_arn    = "${aws_sfn_state_machine.debug_flow.id}"
}

output "debug" {
  value = {
    arn = "${aws_sfn_state_machine.debug_flow.id}"
    trigger_policy = "${module.debug_flow_policies.info["trigger"]}"
  }
}
