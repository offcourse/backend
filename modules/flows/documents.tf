data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]

    principals = [
      {
        type = "Service"
        identifiers = [
          "states.amazonaws.com",
          "lambda.amazonaws.com",
        ]
      },
    ]
  }
}
data "aws_iam_policy_document" "flow" {
  statement {
    actions = [
      "lambda:InvokeFunction",
    ]
    resources = [
      "${lookup(var.services["echo"], "arn")}",
      "${lookup(var.services["save"], "arn")}",
      "${lookup(var.services["remove"], "arn")}",
      "${lookup(var.services["transform"], "arn")}",
      "${lookup(var.services["retrieve"], "arn")}"
    ]
  }
}
