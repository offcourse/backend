data "template_file" "import_course_flow" {
  template =  "${file("${var.templates_dir}/import-course-flow.json.tpl")}"

  vars {
    save      = "${lookup(var.services["save"], "arn")}"
    transform = "${lookup(var.services["transform"], "arn")}"
    retrieve  = "${lookup(var.services["retrieve"], "arn")}"
  }
}

resource "aws_sfn_state_machine" "import_course_flow" {
  name       = "${var.application}-import-course"
  role_arn   = "${aws_iam_role.flows.arn}"
  definition = "${data.template_file.import_course_flow.rendered}"
}

module import_course_flow_policies {
  source      = "./policies"
  application = "${var.application}"
  flow_name   = "import_course"
  flow_arn    = "${aws_sfn_state_machine.import_course_flow.id}"
}

output "import_course" {
  value = {
    arn = "${aws_sfn_state_machine.import_course_flow.id}"
    trigger_policy = "${module.import_course_flow_policies.info["trigger"]}"
  }
}
