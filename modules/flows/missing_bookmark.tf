data "template_file" "missing_bookmark_flow" {
  template =  "${file("${var.templates_dir}/missing-bookmark-flow.json.tpl")}"

  vars {
    save      = "${lookup(var.services["save"], "arn")}"
    transform = "${lookup(var.services["transform"], "arn")}"
    retrieve  = "${lookup(var.services["retrieve"], "arn")}"
  }
}

resource "aws_sfn_state_machine" "missing_bookmark_flow" {
  name       = "${var.application}-missing-bookmark-flow"
  role_arn   = "${aws_iam_role.flows.arn}"
  definition = "${data.template_file.missing_bookmark_flow.rendered}"
}

module missing_bookmark_flow_policies {
  source      = "./policies"
  application = "${var.application}"
  flow_name   = "missing_bookmark"
  flow_arn    = "${aws_sfn_state_machine.missing_bookmark_flow.id}"
}

output "missing_bookmark" {
  value = {
    arn = "${aws_sfn_state_machine.missing_bookmark_flow.id}"
    trigger_policy = "${module.missing_bookmark_flow_policies.info["trigger"]}"
  }
}
