data "template_file" "index_courses_flow" {
  template =  "${file("${var.templates_dir}/index-courses-flow.json.tpl")}"

  vars {
    save = "${lookup(var.services["save"], "arn")}"
  }
}

resource "aws_sfn_state_machine" "index_courses_flow" {
  name       = "${var.application}-index-courses"
  role_arn   = "${aws_iam_role.flows.arn}"
  definition = "${data.template_file.index_courses_flow.rendered}"
}

module index_courses_flow_policies {
  source      = "./policies"
  application = "${var.application}"
  flow_name   = "index_courses"
  flow_arn    = "${aws_sfn_state_machine.index_courses_flow.id}"
}

output "index_courses" {
  value = {
    arn = "${aws_sfn_state_machine.index_courses_flow.id}"
    trigger_policy = "${module.index_courses_flow_policies.info["trigger"]}"
  }
}
