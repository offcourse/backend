data "template_file" "save_status_flow" {
  template =  "${file("${var.templates_dir}/save-status-flow.json.tpl")}"

  vars {
    save = "${lookup(var.services["save"], "arn")}"
  }
}

resource "aws_sfn_state_machine" "save_status_flow" {
  name       = "${var.application}-save-status"
  role_arn   = "${aws_iam_role.flows.arn}"
  definition = "${data.template_file.save_status_flow.rendered}"
}

module save_status_flow_policies {
  source      = "./policies"
  application = "${var.application}"
  flow_name   = "save_status"
  flow_arn    = "${aws_sfn_state_machine.save_status_flow.id}"
}

output "save_status" {
  value = {
    arn = "${aws_sfn_state_machine.save_status_flow.id}"
    trigger_policy = "${module.save_status_flow_policies.info["trigger"]}"
  }
}
