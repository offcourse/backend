data "template_file" "process_course_flow" {
  template =  "${file("${var.templates_dir}/process-course-flow.json.tpl")}"

  vars {
    save      = "${lookup(var.services["save"], "arn")}"
    transform = "${lookup(var.services["transform"], "arn")}"
    retrieve  = "${lookup(var.services["retrieve"], "arn")}"
  }
}

resource "aws_sfn_state_machine" "process_course_flow" {
  name       = "${var.application}-process-course-flow"
  role_arn   = "${aws_iam_role.flows.arn}"
  definition = "${data.template_file.process_course_flow.rendered}"
}

module process_course_flow_policies {
  source      = "./policies"
  application = "${var.application}"
  flow_name   = "process_course"
  flow_arn    = "${aws_sfn_state_machine.process_course_flow.id}"
}

output "process_course" {
  value = {
    arn = "${aws_sfn_state_machine.process_course_flow.id}"
    trigger_policy = "${module.process_course_flow_policies.info["trigger"]}"
  }
}
