resource "aws_iam_policy" "trigger" {
  name   = "${var.application}-${var.flow_name}_trigger"
  policy = "${data.aws_iam_policy_document.trigger.json}"
}
