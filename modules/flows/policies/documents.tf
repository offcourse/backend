data "aws_iam_policy_document" "trigger" {
  statement {
    actions = [
      "states:StartExecution"
    ]
    resources = [
      "${var.flow_arn}"
    ]
  }
}
