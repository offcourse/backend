output "info" {
  value = {
    trigger   = "${data.aws_iam_policy_document.trigger.json}"
  }
}
