data "template_file" "remove_courses_flow" {
  template =  "${file("${var.templates_dir}/remove-courses-flow.json.tpl")}"

  vars {
    remove = "${lookup(var.services["remove"], "arn")}"
  }
}

resource "aws_sfn_state_machine" "remove_courses_flow" {
  name       = "${var.application}-remove-courses"
  role_arn   = "${aws_iam_role.flows.arn}"
  definition = "${data.template_file.remove_courses_flow.rendered}"
}

module remove_courses_flow_policies {
  source      = "./policies"
  application = "${var.application}"
  flow_name   = "remove_courses"
  flow_arn    = "${aws_sfn_state_machine.remove_courses_flow.id}"
}

output "remove_courses" {
  value = {
    arn = "${aws_sfn_state_machine.remove_courses_flow.id}"
    trigger_policy = "${module.remove_courses_flow_policies.info["trigger"]}"
  }
}
