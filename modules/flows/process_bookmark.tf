data "template_file" "process_bookmark_flow" {
  template =  "${file("${var.templates_dir}/process-bookmark-flow.json.tpl")}"

  vars {
    save      = "${lookup(var.services["save"], "arn")}"
    transform = "${lookup(var.services["transform"], "arn")}"
    retrieve  = "${lookup(var.services["retrieve"], "arn")}"
  }
}

resource "aws_sfn_state_machine" "process_bookmark_flow" {
  name       = "${var.application}-process-bookmark-flow"
  role_arn   = "${aws_iam_role.flows.arn}"
  definition = "${data.template_file.process_bookmark_flow.rendered}"
}

module process_bookmark_flow_policies {
  source      = "./policies"
  application = "${var.application}"
  flow_name   = "process_bookmark"
  flow_arn    = "${aws_sfn_state_machine.process_bookmark_flow.id}"
}

output "process_bookmark" {
  value = {
    arn = "${aws_sfn_state_machine.process_bookmark_flow.id}"
    trigger_policy = "${module.process_bookmark_flow_policies.info["trigger"]}"
  }
}
