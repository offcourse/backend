output info {
  value = {
    arn            = "${aws_sns_topic.failed_events.arn}"
    publish_policy = "${data.aws_iam_policy_document.publish.json}"
  }
}
