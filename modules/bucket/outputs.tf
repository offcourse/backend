output "info" {
  value = {
    name          = "${aws_s3_bucket.bucket.id}"
    arn           = "${aws_s3_bucket.bucket.arn}"
    read_policy   = "${data.aws_iam_policy_document.read.json}"
    write_policy  = "${data.aws_iam_policy_document.write.json}"
    full_policy  = "${data.aws_iam_policy_document.full.json}"
  }
}
