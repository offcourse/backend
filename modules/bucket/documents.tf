data "aws_iam_policy_document" "write" {
  statement {
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "${aws_s3_bucket.bucket.arn}/*"
    ]
  }
}

data "aws_iam_policy_document" "read" {
  statement {
    actions = [
      "s3:getObject"
    ]
    resources = [
      "${aws_s3_bucket.bucket.arn}",
      "${aws_s3_bucket.bucket.arn}/*"
    ]
  }
}

data "aws_iam_policy_document" "full" {
  statement {
    actions = [
      "s3:AbortMultipartUpload",        
      "s3:GetBucketLocation",        
      "s3:GetObject",        
      "s3:ListBucket",        
      "s3:ListBucketMultipartUploads",        
      "s3:PutObject"
    ]
    resources = [
      "${aws_s3_bucket.bucket.arn}",
      "${aws_s3_bucket.bucket.arn}/*"
    ]
  }
}