resource "aws_s3_bucket" "bucket" {
  bucket        = "${var.application}-${var.bucket_name}"
  acl           = "public-read"

  tags {
    Application = "${var.application}"
  }
}
