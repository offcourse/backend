resource "aws_api_gateway_account" "API" {
  cloudwatch_role_arn = "${aws_iam_role.api.arn}"
}

resource "aws_api_gateway_rest_api" "API" {
  name = "${var.application}-API"
}

resource "aws_api_gateway_domain_name" "api" {
  domain_name     = "${local.domain_name}"
  certificate_arn = "${var.zone["certificate_arn"]}"
}

resource "aws_route53_record" "api" {
  zone_id = "${var.zone["zone_id"]}"
  name    = "${aws_api_gateway_domain_name.api.domain_name}"
  type    = "A"

  alias {
    name                   = "${aws_api_gateway_domain_name.api.cloudfront_domain_name}"
    zone_id                = "${aws_api_gateway_domain_name.api.cloudfront_zone_id}"
    evaluate_target_health = true
  }
}
resource "aws_api_gateway_base_path_mapping" "default" {
  depends_on = [
    "aws_api_gateway_domain_name.api"
  ]
  api_id      = "${aws_api_gateway_rest_api.API.id}"
  stage_name  = "${aws_api_gateway_deployment.v1.stage_name}"
  domain_name = "${local.domain_name}"
}

resource "aws_lambda_permission" "lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${lookup(var.services["graphql"], "arn")}"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${var.region}:${var.identity}:${aws_api_gateway_rest_api.API.id}/*"
}