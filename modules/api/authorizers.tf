resource "aws_cloudformation_stack" "user_pool_authorizer" {
  name = "user-pool-authorizer"
  template_body = <<STACK
  {
  "AWSTemplateFormatVersion": "2010-09-09",
  "Resources": {
    "CognitoAuthorizer": {
      "Type": "AWS::ApiGateway::Authorizer",
      "Properties": {
        "IdentitySource": "method.request.header.Authorization",
        "Name": "CognitoAuthorizer",
        "ProviderARNs": ["arn:aws:cognito-idp:${var.region}:${var.identity}:userpool/${var.user_pool}"],
        "RestApiId": "${aws_api_gateway_rest_api.API.id}",
        "Type": "COGNITO_USER_POOLS"
      }
    }
  },
  "Outputs": {
    "AuthorizerId": {"Value": { "Ref": "CognitoAuthorizer" }}
  }
}
STACK
}

resource "aws_api_gateway_authorizer" "offcourse_authorizer" {
  name                   = "offcourse-authorizer"
  rest_api_id            = "${aws_api_gateway_rest_api.API.id}"
  authorizer_uri         = "${lookup(var.services["authorize"], "invoke_arn")}"
  authorizer_credentials = "${aws_iam_role.api.arn}"
}
