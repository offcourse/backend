data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]

    principals = [
      {
        type = "Service"
        identifiers = [
          "lambda.amazonaws.com",
          "apigateway.amazonaws.com"
        ]
      },
    ]
  }
}

data "aws_iam_policy_document" "monitoring" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:GetLogEvents",
      "logs:FilterLogEvents"
    ],
    resources = [
      "*"
    ]
  }
}

data "aws_iam_policy_document" "api_invocation" {
  statement {
    actions = [
      "lambda:InvokeFunction"
    ],
    resources = [
      "${lookup(var.services["authorize"], "arn")}"
    ]
  }
}
