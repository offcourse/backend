resource "aws_iam_role" "api" {
  name = "${var.application}-api"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role.json}"
}

resource "aws_iam_role_policy" "api_monitoring" {
  name = "${var.application}-api-monitoring"
  role = "${aws_iam_role.api.id}"
  policy = "${data.aws_iam_policy_document.monitoring.json}"
}

resource "aws_iam_role_policy" "api_invocation" {
  name = "${var.application}-api-invocation"
  role = "${aws_iam_role.api.id}"
  policy = "${data.aws_iam_policy_document.api_invocation.json}"
}
