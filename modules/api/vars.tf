variable application {}
variable region {}
variable identity {}

variable zone {
  type = "map"
}

variable user_pool {}

variable services {
  type = "map"
}

locals  {
  stage_name = "v1"
  canary_stage_name = "canary"
  domain_name  = "api.${var.zone["domain_name"]}"
}
