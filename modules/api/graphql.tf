resource "aws_api_gateway_resource" "graphql" {
  rest_api_id = "${aws_api_gateway_rest_api.API.id}"
  parent_id   = "${aws_api_gateway_rest_api.API.root_resource_id}"
  path_part   = "graphql"
}

module "graphql_cors" {
  source            = "./cors"
  api_id            = "${aws_api_gateway_rest_api.API.id}"
  resource_id       = "${aws_api_gateway_resource.graphql.id}"
}

resource "aws_api_gateway_method" "graphql" {
  rest_api_id   = "${aws_api_gateway_rest_api.API.id}"
  resource_id   = "${aws_api_gateway_resource.graphql.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.offcourse_authorizer.id}"
}

resource "aws_api_gateway_integration" "graphql" {
  rest_api_id             = "${aws_api_gateway_rest_api.API.id}"
  resource_id             = "${aws_api_gateway_resource.graphql.id}"
  http_method             = "${aws_api_gateway_method.graphql.http_method}"
  integration_http_method = "POST"
  content_handling        = "CONVERT_TO_TEXT"
  uri                     = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/${lookup(var.services["graphql"], "production_arn")}/invocations"
  type                    = "AWS_PROXY"
}

resource "aws_api_gateway_method_settings" "graphql" {
  depends_on = [
    "aws_api_gateway_base_path_mapping.default"
  ]
  rest_api_id = "${aws_api_gateway_rest_api.API.id}"
  stage_name  = "${local.stage_name}"
  method_path = "${aws_api_gateway_resource.graphql.path_part}/${aws_api_gateway_method.graphql.http_method}"

  settings {
    metrics_enabled = true
    data_trace_enabled = true
    logging_level   = "INFO"
  }
}