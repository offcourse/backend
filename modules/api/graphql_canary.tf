resource "aws_api_gateway_resource" "graphql_canary" {
  rest_api_id = "${aws_api_gateway_rest_api.API.id}"
  parent_id   = "${aws_api_gateway_rest_api.API.root_resource_id}"
  path_part   = "canary"
}

module "graphql_canary_cors" {
  source            = "./cors"
  api_id            = "${aws_api_gateway_rest_api.API.id}"
  resource_id       = "${aws_api_gateway_resource.graphql_canary.id}"
}

resource "aws_api_gateway_method" "graphql_canary" {
  rest_api_id   = "${aws_api_gateway_rest_api.API.id}"
  resource_id   = "${aws_api_gateway_resource.graphql_canary.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.offcourse_authorizer.id}"
}

resource "aws_api_gateway_integration" "graphql_canary" {
  rest_api_id             = "${aws_api_gateway_rest_api.API.id}"
  resource_id             = "${aws_api_gateway_resource.graphql_canary.id}"
  http_method             = "${aws_api_gateway_method.graphql_canary.http_method}"
  content_handling        = "CONVERT_TO_TEXT"
  integration_http_method = "POST"
  uri                     = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/${lookup(var.services["graphql"], "arn")}/invocations"
  type                    = "AWS_PROXY"
}

resource "aws_api_gateway_method_settings" "graphql_canary" {
  depends_on = [
    "aws_api_gateway_base_path_mapping.default"
  ]
  rest_api_id = "${aws_api_gateway_rest_api.API.id}"
  stage_name  = "${local.stage_name}"
  method_path = "${aws_api_gateway_resource.graphql_canary.path_part}/${aws_api_gateway_method.graphql_canary.http_method}"

  settings {
    metrics_enabled = true
    data_trace_enabled = true
    logging_level   = "INFO"
  }
}