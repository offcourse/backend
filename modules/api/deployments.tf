resource "aws_api_gateway_deployment" "v1" {
  depends_on = [
    "aws_api_gateway_integration.graphql",
    "aws_api_gateway_integration.graphql_canary",
  ]
  rest_api_id = "${aws_api_gateway_rest_api.API.id}"
  stage_name = "${local.stage_name}"
}