resource "aws_dynamodb_table" "courses" {
  name             = "${var.application}-courses"
  read_capacity    = 10
  write_capacity   = 10
  hash_key         = "course-id"
  range_key        = "revision"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"
  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "course-id"
    type = "S"
  }

  attribute {
    name = "revision"
    type = "N"
  }

  attribute {
    name = "curator"
    type = "S"
  }

  attribute {
    name = "goal"
    type = "S"
  }

  global_secondary_index {
    name               = "curator-courses"
    hash_key           = "curator"
    range_key          = "goal"
    write_capacity     = 1
    read_capacity      = 1
    projection_type    = "KEYS_ONLY"
  }
}

module courses_policies {
  source      = "./policies"
  application = "${var.application}"
  table_name  = "${aws_dynamodb_table.courses.id}"
  table_arn   = "${aws_dynamodb_table.courses.arn}"
  stream_arn  = "${aws_dynamodb_table.courses.stream_arn}"
}
