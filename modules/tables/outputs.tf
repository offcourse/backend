output courses {
  value = {
    name          = "${aws_dynamodb_table.courses.id}"
    arn           = "${aws_dynamodb_table.courses.arn}"
    stream_arn    = "${aws_dynamodb_table.courses.stream_arn}"
    read_policy   = "${module.courses_policies.info["read"]}"
    scan_policy   = "${module.courses_policies.info["scan"]}"
    write_policy  = "${module.courses_policies.info["write"]}"
    stream_policy = "${module.courses_policies.info["stream"]}"
  }
}

output statuses {
  value = {
    name          = "${aws_dynamodb_table.statuses.id}"
    arn           = "${aws_dynamodb_table.statuses.arn}"
    stream_arn    = "${aws_dynamodb_table.statuses.stream_arn}"
    read_policy   = "${module.statuses_policies.info["read"]}"
    scan_policy   = "${module.courses_policies.info["scan"]}"
    write_policy  = "${module.statuses_policies.info["write"]}"
    stream_policy = "${module.statuses_policies.info["stream"]}"
  }
}
