output "info" {
  value = {
    read   = "${data.aws_iam_policy_document.read.json}"
    write  = "${data.aws_iam_policy_document.write.json}"
    stream = "${data.aws_iam_policy_document.stream.json}"
    scan = "${data.aws_iam_policy_document.scan.json}"
  }
}
