data "aws_iam_policy_document" "write" {
  statement {
    actions = [
      "dynamodb:PutItem",
      "dynamodb:UpdateItem",
      "dynamodb:BatchWriteItem"
    ]
    resources = [
      "${var.table_arn}"
    ]
  }
}

data "aws_iam_policy_document" "read" {
  statement {
    actions = [
      "dynamodb:Query",
      "dynamodb:GetRecords"
    ]
    resources = [
      "${var.table_arn}",
      "${var.table_arn}/index/*"
    ]
  }
}
data "aws_iam_policy_document" "scan" {
  statement {
    actions = [
      "dynamodb:DescribeTable",
      "dynamodb:Scan"
    ]
    resources = [
      "${var.table_arn}"
    ]
  }
}

data "aws_iam_policy_document" "stream" {
  statement {
    actions = [
      "dynamodb:GetRecords",
      "dynamodb:GetShardIterator",
      "dynamodb:DescribeStream",
      "dynamodb:ListStreams"
    ]
    resources = [
      "${var.stream_arn}"
    ]
  }
}
