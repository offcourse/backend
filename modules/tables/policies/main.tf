resource "aws_iam_policy" "stream" {
  name   = "${var.table_name}_table_stream"
  name   = "${var.application}-${var.table_name}_table_stream"
  policy = "${data.aws_iam_policy_document.stream.json}"
}

resource "aws_iam_policy" "write" {
  name   = "${var.application}-${var.table_name}_table_write"
  policy = "${data.aws_iam_policy_document.write.json}"
}

resource "aws_iam_policy" "read" {
  name   = "${var.application}-${var.table_name}_table_read"
  policy = "${data.aws_iam_policy_document.read.json}"
}

resource "aws_iam_policy" "scan" {
  name   = "${var.application}-${var.table_name}_table_scan"
  policy = "${data.aws_iam_policy_document.scan.json}"
}
