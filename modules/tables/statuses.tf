resource "aws_dynamodb_table" "statuses" {
  name             = "${var.application}-statuses"
  read_capacity    = 10
  write_capacity   = 10
  hash_key         = "learner"
  range_key        = "course-id"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "course-id"
    type = "S"
  }

  attribute {
    name = "learner"
    type = "S"
  }
}

module statuses_policies {
  source      = "./policies"
  application = "${var.application}"
  table_name  = "${aws_dynamodb_table.statuses.id}"
  table_arn   = "${aws_dynamodb_table.statuses.arn}"
  stream_arn  = "${aws_dynamodb_table.statuses.stream_arn}"
}
