resource "aws_elasticsearch_domain" "domain" {
  domain_name = "${var.application}-${var.domain_name}"
  elasticsearch_version = "6.0"

  cluster_config {
    instance_type = "t2.medium.elasticsearch"
    instance_count = 1
  }

  ebs_options {
    ebs_enabled = true
    volume_size = 10
    volume_type = "gp2"
  }

  tags {
    Application = "${var.application}"
  }
}
