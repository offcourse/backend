output "info" {
  value = {
    arn          = "${aws_elasticsearch_domain.domain.arn}"
    endpoint     = "https://${aws_elasticsearch_domain.domain.endpoint}"
    kibana       = "https://${aws_elasticsearch_domain.domain.endpoint}/_plugin/kibana/"
    read_policy   = "${data.aws_iam_policy_document.read.json}"
    write_policy  = "${data.aws_iam_policy_document.write.json}"
    delete_policy  = "${data.aws_iam_policy_document.delete.json}"
  }
}
