data "aws_iam_policy_document" "es" {
  statement {
    sid = "admin"
    resources = [
      "${aws_elasticsearch_domain.domain.arn}/*"
    ]
    principals = [
      {
        type        = "AWS"
        identifiers = ["*"]
      },
    ]
    actions = [
      "es:*"
    ]
    condition {
      test = "IpAddress"
      variable = "aws:SourceIp"
      values = [
        "82.95.194.172"
      ]
    }
  }

  statement {
    sid = "client"
    resources = [
      "${aws_elasticsearch_domain.domain.arn}/*"
    ]
    principals = [
      {
        type        = "AWS"
        identifiers = ["*"]
      },
    ]
    actions = [
      "es:ESHttpGet"
    ]
  }
}

data "aws_iam_policy_document" "write" {
  statement {
    actions = [
      "es:ESHttpPost"
    ]
    resources = [
      "${aws_elasticsearch_domain.domain.arn}/*"
    ]
  }
}

data "aws_iam_policy_document" "read" {
  statement {
    actions = [
      "es:ESHttpGet",
      "es:ESHttpPost"
    ]
    resources = [
      "${aws_elasticsearch_domain.domain.arn}/*"
    ]
  }
}

data "aws_iam_policy_document" "delete" {
  statement {
    actions = [
      "es:ESHttpDelete",
    ]
    resources = [
      "${aws_elasticsearch_domain.domain.arn}/*"
    ]
  }
}
