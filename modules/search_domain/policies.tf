resource "aws_elasticsearch_domain_policy" "main" {
  domain_name = "${aws_elasticsearch_domain.domain.domain_name}"
  access_policies = "${data.aws_iam_policy_document.es.json}"
}

resource "aws_iam_policy" "write" {
  name   = "${var.application}-${var.domain_name}_domain_write"
  policy = "${data.aws_iam_policy_document.write.json}"
}

resource "aws_iam_policy" "read" {
  name   = "${var.application}-${var.domain_name}_domain_read"
  policy = "${data.aws_iam_policy_document.read.json}"
}
