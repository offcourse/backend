
output next {
  value = "${module.next.info}"
}
output contribute_next {
  value = "${module.contribute_next.info}"
}
