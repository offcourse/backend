variable application {}

variable edge_functions {
  type = "map"
}

variable zone {
  type = "map"
}

locals {
  naked_alias = "${var.zone["domain_name"]}"
  domain_name = "www.${var.zone["domain_name"]}"
}
