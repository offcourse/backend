module next {
  source       = "../../modules/site"
  site_name    = "next"
  application  = "${var.application}"

  zone = {
    domain_name     = "${var.zone["domain_name"]}"
    zone_id         = "${var.zone["zone_id"]}"
    certificate_arn = "${var.zone["certificate_arn"]}"
  }
}
module beta {
  source       = "../../modules/site-with-edge-function"
  site_name    = "beta"
  application  = "${var.application}"
  redirect     = false

  zone = {
    domain_name     = "${var.zone["domain_name"]}"
    zone_id         = "${var.zone["zone_id"]}"
    certificate_arn = "${var.zone["certificate_arn"]}"
  }

  edge_functions = {
    redirect = "${var.edge_functions["redirect"]}"
  }
}
module stable {
  source       = "../../modules/site"
  site_name    = "stable"
  application  = "${var.application}"
  needs_prefix = false

  zone = {
    domain_name     = "${var.zone["domain_name"]}"
    zone_id         = "${var.zone["zone_id"]}"
    certificate_arn = "${var.zone["certificate_arn"]}"
  }
}

module contribute_next {
  source       = "../../modules/site"
  site_name    = "contribute-next"
  application  = "${var.application}"

  zone = {
    domain_name     = "${var.zone["domain_name"]}"
    zone_id         = "${var.zone["zone_id"]}"
    certificate_arn = "${var.zone["certificate_arn"]}"
  }
}

resource "aws_route53_record" "naked" {
  zone_id = "${var.zone["zone_id"]}"
  name    = "${local.naked_alias}"
  type    = "A"

  alias {
    name                   = "${module.stable.info["endpoint"]}"
    zone_id                = "${module.stable.info["hosted_zone_id"]}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "www" {
  zone_id = "${var.zone["zone_id"]}"
  name    = "${local.domain_name}"
  type    = "A"

  alias {
    name                   = "${module.stable.info["endpoint"]}"
    zone_id                = "${module.stable.info["hosted_zone_id"]}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "contribute" {
  zone_id = "${var.zone["zone_id"]}"
  name    = "contribute.${var.zone["domain_name"]}"
  type    = "CNAME"
  ttl     = 300
  records = ["condescending-wing-149611.netlify.com"]
}
