module mappings {
  source        = "../mappings"
  application   = "${var.application}"

  tables {
    courses     = "${module.tables.courses}"
    statuses    = "${module.tables.statuses}"
  }

  services {
    distribute = "${module.services.distribute}"
    echo       = "${module.services.echo}"
  }

  buckets {
    temp        = "${module.buckets.temp}"
    bookmarks   = "${module.buckets.bookmarks}"
  }
}
