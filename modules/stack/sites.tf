module sites {
  source         = "../sites"
  application    = "${var.application}"

  zone = {
    domain_name     = "${var.zone["domain_name"]}"
    zone_id         = "${var.zone["zone_id"]}"
    certificate_arn = "${var.zone["certificate_arn"]}"
  }

  edge_functions = {
    redirect     = "${module.edge_functions.redirect}"
  }
}
