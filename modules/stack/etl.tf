module etl {
  source      = "../etl"
  application = "${var.application}"
  buckets = {
    stream_events = "${module.buckets.stream_events}"
    course_collections = "${module.buckets.course_collections}"
  }
  tables = {
    courses = "${module.tables.courses}"
  }
}
