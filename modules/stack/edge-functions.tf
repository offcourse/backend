module edge_functions {
  source      = "../edge-functions"
  application = "${var.application}"
  artifacts_dir = "./artifacts/functions"
}
