module search_cluster {
  source      = "../search_domain"
  application = "${var.application}"
  domain_name = "search"
}
