variable application {}
variable region {}

variable api_keys {
  type = "map"
}

variable zone {
  type = "map"
}

data "aws_caller_identity" "current" {}

locals {
  account_id = "${data.aws_caller_identity.current.account_id}"
}
