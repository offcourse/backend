module api {
  source      = "../api"
  application = "${var.application}"
  region      = "${var.region}"
  identity    = "${local.account_id}"
  user_pool   = "${module.auth.user_pool["id"]}"

  zone = {
    domain_name     = "${var.zone["domain_name"]}"
    zone_id         = "${var.zone["zone_id"]}"
    certificate_arn = "${var.zone["certificate_arn"]}"
  }

  services = {
    authorize = "${module.services.authorize}"
    graphql   = "${module.services.graphql}"
  }
}
