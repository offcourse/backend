module services {
  source      = "../services"
  application = "${var.application}"
  artifacts_dir = "./artifacts/functions"

  api_keys = {
    github                 = "${var.api_keys["github"]}"
    mercury                = "${var.api_keys["mercury"]}"
    algolia_application_id = "${var.api_keys["algolia_application_id"]}"
    algolia_admin_key      = "${var.api_keys["algolia_admin_key"]}"
  }

  auth = {
    user_pool = "${module.auth.user_pool}"
  }

  tables = {
    courses    = "${module.tables.courses}"
    statuses   = "${module.tables.statuses}"
  }

  buckets = {
    raw_resources       = "${module.buckets.raw_resources}"
    bookmarks           = "${module.buckets.bookmarks}"
    resources           = "${module.buckets.resources}"
    temp                = "${module.buckets.temp}"
    statuses            = "${module.buckets.statuses}"
    assets              = "${module.buckets.assets}"
    course_collections  = "${module.buckets.course_collections}"
    stream_events       = "${module.buckets.stream_events}"
    big_table_artifacts = "${module.buckets.big_table_artifacts}"
  }

  search_clusters = {
    data          = "${module.search_cluster.info}"
  }

  flows = {
    debug            = "${module.flows.debug}"
    import_repo      = "${module.flows.import_repo}"
    import_course    = "${module.flows.import_course}"
    process_bookmark = "${module.flows.process_bookmark}"
    process_course   = "${module.flows.process_course}"
    missing_bookmark = "${module.flows.missing_bookmark}"
    index_courses    = "${module.flows.index_courses}"
    remove_courses   = "${module.flows.remove_courses}"
    save_status      = "${module.flows.save_status}"
  }

  big_tables = {
    events = "${module.etl.info}"
  }

  topics = {
    failed_events = "${module.topics.info}"
  }
  firehoses = {
    raw_events    = "${module.firehoses.raw_events}"
    stream_events = "${module.firehoses.stream_events}"
  }
}
