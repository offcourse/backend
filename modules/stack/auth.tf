module auth {
  source      = "../auth"
  application = "${var.application}"
  region      = "${var.region}"
  identity    = "${local.account_id}"
  domain_name = "${var.zone["domain_name"]}"
}
