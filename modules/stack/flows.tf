module flows {
  source        = "../flows"
  application = "${var.application}"
  templates_dir = "./artifacts/flows"

  services = {
    save       = "${module.services.save}"
    transform  = "${module.services.transform}"
    retrieve   = "${module.services.retrieve}"
    echo       = "${module.services.echo}"
    remove     = "${module.services.remove}"
  }
}
