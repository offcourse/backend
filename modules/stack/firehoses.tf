
module firehoses {
  source      = "../firehoses"
  application = "${var.application}"

  buckets = {
    raw_events = "${module.buckets.raw_events}"
    stream_events = "${module.buckets.stream_events}"
  }
}