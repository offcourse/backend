module buckets {
  source      = "../buckets"
  application = "${var.application}"

  zone = {
    domain_name     = "${var.zone["domain_name"]}"
    zone_id         = "${var.zone["zone_id"]}"
    certificate_arn = "${var.zone["certificate_arn"]}"
  }
}
