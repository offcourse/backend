module raw_resources {
  source      = "../bucket"
  application = "${var.application}"
  bucket_name = "raw-resources"
}

module bookmarks {
  source      = "../bucket"
  application = "${var.application}"
  bucket_name = "bookmarks"
}

module resources {
  source      = "../bucket"
  application = "${var.application}"
  bucket_name = "resources"
}


module statuses {
  source      = "../bucket"
  application = "${var.application}"
  bucket_name = "statuses"
}
module big_table_artifacts {
  source      = "../bucket"
  application = "${var.application}"
  bucket_name = "big-table-artifacts"
}

module temp {
  source      = "../bucket"
  application = "${var.application}"
  bucket_name = "incoming"
}

module assets {
  source      = "../../modules/site"
  application = "${var.application}"
  site_name   = "assets"
  zone        = "${var.zone}"
}

module raw_events {
  source      = "../bucket"
  application = "${var.application}"
  bucket_name = "raw-events"
}
module stream_events {
  source      = "../bucket"
  application = "${var.application}"
  bucket_name = "stream-events"
}
module course_collections {
  source      = "../bucket"
  application = "${var.application}"
  bucket_name = "course-collections"
}