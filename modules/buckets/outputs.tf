output raw_resources {
  value = "${module.raw_resources.info}"
}

output raw_events {
  value = "${module.raw_events.info}"
}
output stream_events {
  value = "${module.stream_events.info}"
}

output bookmarks {
  value = "${module.bookmarks.info}"
}

output resources {
  value = "${module.resources.info}"
}

output statuses {
  value = "${module.statuses.info}"
}

output temp {
  value = "${module.temp.info}"
}

output assets {
  value = "${module.assets.info}"
}

output course_collections {
  value = "${module.course_collections.info}"
}

output big_table_artifacts {
  value = "${module.big_table_artifacts.info}"
}