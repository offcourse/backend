module raw_events {
  source            = "../firehose"
  application       = "${var.application}"
  stream_name       = "raw-events"
  bucket            = "${var.buckets["raw_events"]}"
}

module stream_events {
  source            = "../firehose"
  application       = "${var.application}"
  stream_name       = "stream-events"
  bucket            = "${var.buckets["stream_events"]}"
}

output raw_events {
  value = "${module.raw_events.info}"
}

output stream_events {
  value = "${module.stream_events.info}"
}