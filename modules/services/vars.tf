variable application {}

variable api_keys {
  type = "map"
}

variable tables {
  type = "map"
}

variable buckets {
  type = "map"
}

variable flows {
  type = "map"
}

variable auth {
  type = "map"
}

variable topics {
  type = "map"
}

variable search_clusters {
  type = "map"
}

variable firehoses {
  type = "map"
}
variable big_tables {
  type = "map"
}

variable artifacts_dir {}

variable environment_prefixes {
  default = {
    development = "nightly",
    staging     = "edge",
    production  = "platform"
  }
}
