resource "aws_lambda_function" "authorize" {
  function_name    = "${var.application}-authorize"
  handler          = "index.handler"
  runtime          = "nodejs6.10"
  filename         = "${var.file_name}"
  source_code_hash = "${base64sha256(file("${var.file_name}"))}"
  role             = "${aws_iam_role.authorize.arn}"
  memory_size      = 256
  timeout          = 20

  tracing_config {
    mode = "Active"
  }

  dead_letter_config {
    target_arn = "${lookup(var.topics["failed_events"], "arn")}"
  }

  environment {
    variables {
      cognito_user_pool_iss = "${lookup(var.auth["user_pool"], "iss")}"
    }
  }
}
