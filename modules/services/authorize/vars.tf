variable application {}
variable file_name {}

variable topics {
  type = "map"
}

variable auth {
  type = "map"
}

variable policy_documents {
  type = "map"
}
