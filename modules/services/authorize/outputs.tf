output info {
  value = {
    arn = "${aws_lambda_function.authorize.arn}"
    invoke_arn = "${aws_lambda_function.authorize.invoke_arn}"
  }
}
