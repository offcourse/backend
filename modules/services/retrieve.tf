module retrieve {
  source      = "./retrieve"
  application = "${var.application}"
  file_name   = "${var.artifacts_dir}/retrieve-0.1.0-SNAPSHOT.zip"

  api_keys = {
    github  = "${var.api_keys["github"]}"
    mercury = "${var.api_keys["mercury"]}"
  }

  buckets = {
    raw_resources = "${var.buckets["raw_resources"]}"
    resources     = "${var.buckets["resources"]}"
  }

  tables = {
    courses = "${var.tables["courses"]}"
  }

  topics = {
    failed_events  = "${var.topics.["failed_events"]}"
  }

  policy_documents = {
    assume_role = "${data.aws_iam_policy_document.assume_role.json}"
    monitoring  = "${data.aws_iam_policy_document.monitoring.json}"
  }
}
