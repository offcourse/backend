resource "aws_iam_role" "remove" {
  name = "${var.application}-remove_service"
  assume_role_policy = "${var.policy_documents["assume_role"]}"
}

resource "aws_iam_role_policy" "search_cluster_write" {
  name   = "search_cluster_write"
  role   = "${aws_iam_role.remove.name}"
  policy = "${lookup(var.search_clusters["data"], "write_policy")}"
}

resource "aws_iam_role_policy" "failed_events_publish" {
  name   = "failed_events_publish"
  role   = "${aws_iam_role.remove.name}"
  policy = "${lookup(var.topics["failed_events"], "publish_policy")}"
}

resource "aws_iam_role_policy" "monitoring" {
  name = "monitoring"
  role = "${aws_iam_role.remove.id}"
  policy = "${var.policy_documents["monitoring"]}"
}
