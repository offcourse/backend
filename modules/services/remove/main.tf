resource "aws_lambda_function" "remove" {
  function_name    = "${var.application}-remove"
  handler          = "index.handler"
  runtime          = "nodejs6.10"
  filename         = "${var.file_name}"
  source_code_hash = "${base64sha256(file("${var.file_name}"))}"
  role             = "${aws_iam_role.remove.arn}"
  memory_size      = 256
  timeout          = 60

  tracing_config {
    mode = "Active"
  }

  dead_letter_config {
    target_arn = "${lookup(var.topics["failed_events"], "arn")}"
  }

  environment {
    variables {
      search_cluster         = "${lookup(var.search_clusters["data"], "endpoint")}"
      algolia_application_id = "${var.api_keys["algolia_application_id"]}"
      algolia_admin_key      = "${var.api_keys["algolia_admin_key"]}"
    }
  }
}
