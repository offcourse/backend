variable application {}
variable file_name {}

variable api_keys {
  type = "map"
}

variable topics {
  type = "map"
}

variable search_clusters {
  type = "map"
}

variable policy_documents {
  type = "map"
}
