output info {
  value = {
    arn = "${aws_lambda_function.gather_popular_courses.arn}"
    role_arn = "${aws_iam_role.gather_popular_courses.arn}"
  }
}
