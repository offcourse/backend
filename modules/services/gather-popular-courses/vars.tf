variable application {}
variable file_name {}


variable buckets {
  type = "map"
}

variable search_clusters {
  type = "map"
}

variable topics {
  type = "map"
}

variable policy_documents {
  type = "map"
}

variable big_tables {
  type = "map"
}
