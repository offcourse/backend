resource "aws_iam_role" "gather_popular_courses" {
  name               = "${var.application}-gather_popular_courses_service"
  assume_role_policy = "${var.policy_documents["assume_role"]}"
}

resource "aws_iam_role_policy" "search_cluster_read" {
  name   = "search_cluster_read"
  role   = "${aws_iam_role.gather_popular_courses.name}"
  policy = "${lookup(var.search_clusters["data"], "read_policy")}"
}

resource "aws_iam_role_policy" "search_cluster_write" {
  name   = "search_cluster_write"
  role   = "${aws_iam_role.gather_popular_courses.name}"
  policy = "${lookup(var.search_clusters["data"], "write_policy")}"
}
resource "aws_iam_role_policy" "search_cluster_delete" {
  name   = "search_cluster_delete"
  role   = "${aws_iam_role.gather_popular_courses.name}"
  policy = "${lookup(var.search_clusters["data"], "delete_policy")}"
}
resource "aws_iam_role_policy" "big_table_artifacts_full" {
  name   = "big_table_artifacts_full"
  role   = "${aws_iam_role.gather_popular_courses.name}"
  policy = "${lookup(var.buckets["big_table_artifacts"], "full_policy")}"
}
resource "aws_iam_role_policy" "stream_events_full" {
  name   = "stream_events_full"
  role   = "${aws_iam_role.gather_popular_courses.name}"
  policy = "${lookup(var.buckets["stream_events"], "full_policy")}"
}

resource "aws_iam_role_policy" "big_table_full" {
  name   = "big_table_full"
  role   = "${aws_iam_role.gather_popular_courses.name}"
  policy = "${lookup(var.big_tables["events"], "full_policy")}"
}

resource "aws_iam_role_policy" "monitoring" {
  name   = "monitoring"
  role   = "${aws_iam_role.gather_popular_courses.id}"
  policy = "${var.policy_documents["monitoring"]}"
}

resource "aws_iam_role_policy" "failed_events_publish" {
  name   = "failed_events"
  role   = "${aws_iam_role.gather_popular_courses.name}"
  policy = "${lookup(var.topics["failed_events"], "publish_policy")}"
}