resource "aws_lambda_function" "gather_popular_courses" {
  function_name    = "${var.application}-gather-popular-courses"
  handler          = "index.handler"
  runtime          = "nodejs6.10"
  filename         = "${var.file_name}"
  source_code_hash = "${base64sha256(file("${var.file_name}"))}"
  role             = "${aws_iam_role.gather_popular_courses.arn}"
  memory_size      = 256
  timeout          = 60
  publish          = true

  tracing_config {
    mode = "Active"
  }

  dead_letter_config {
    target_arn = "${lookup(var.topics["failed_events"], "arn")}"
  }

  environment {
    variables {
      search_cluster      = "${lookup(var.search_clusters["data"], "endpoint")}"
      big_table_artifacts = "${lookup(var.buckets["big_table_artifacts"], "name")}"
      events_table        = "${lookup(var.big_tables["events"], "database_name")}"
    }
  }
}
