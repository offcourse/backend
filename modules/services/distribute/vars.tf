variable application {}
variable file_name {}


variable topics {
  type = "map"
}

variable tables {
  type = "map"
}

variable buckets {
  type = "map"
}

variable flows {
  type = "map"
}

variable policy_documents {
  type = "map"
}
