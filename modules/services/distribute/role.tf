resource "aws_iam_role" "distribute" {
  name = "${var.application}-distribute_service"
  assume_role_policy = "${var.policy_documents["assume_role"]}"
}

resource "aws_iam_role_policy" "courses_table_stream" {
  name   = "courses_table_stream"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.tables["courses"], "stream_policy")}",
}

resource "aws_iam_role_policy" "statuses_table_stream" {
  name   = "statuses_table_stream"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.tables["statuses"], "stream_policy")}",
}

resource "aws_iam_role_policy" "temp_bucket_read" {
  name   = "temp_bucket_read"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.buckets["temp"], "read_policy")}"
}

resource "aws_iam_role_policy" "bookmarks_bucket_read" {
  name   = "bookmarks_bucket_read"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.buckets["bookmarks"], "read_policy")}"
}

resource "aws_iam_role_policy" "import_course_flow_trigger" {
  name    = "import_course_flow_trigger"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.flows["import_course"], "trigger_policy")}"
}

resource "aws_iam_role_policy" "process_bookmark_flow_trigger" {
  name    = "process_bookmark_flow_trigger"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.flows["process_bookmark"], "trigger_policy")}"
}

resource "aws_iam_role_policy" "missing_bookmark_flow_trigger" {
  name    = "missing_bookmark_flow_trigger"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.flows["missing_bookmark"], "trigger_policy")}"
}

resource "aws_iam_role_policy" "process_course_flow_trigger" {
  name    = "process_course_flow_trigger"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.flows["process_course"], "trigger_policy")}"
}

resource "aws_iam_role_policy" "index_courses_flow_trigger" {
  name    = "index_courses_flow_trigger"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.flows["index_courses"], "trigger_policy")}"
}

resource "aws_iam_role_policy" "remove_courses_flow_trigger" {
  name    = "remove_courses_flow_trigger"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.flows["remove_courses"], "trigger_policy")}"
}

resource "aws_iam_role_policy" "save_status_flow_trigger" {
  name    = "save_status_flow_trigger"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.flows["save_status"], "trigger_policy")}"
}

resource "aws_iam_role_policy" "failed_events_publish" {
  name   = "failed_events_publish"
  role   = "${aws_iam_role.distribute.name}"
  policy = "${lookup(var.topics["failed_events"], "publish_policy")}"
}

resource "aws_iam_role_policy" "monitoring" {
  name   = "monitoring"
  role   = "${aws_iam_role.distribute.id}"
  policy = "${var.policy_documents["monitoring"]}"
}
