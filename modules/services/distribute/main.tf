resource "aws_lambda_function" "distribute" {
  function_name    = "${var.application}-distribute"
  handler          = "index.handler"
  runtime          = "nodejs6.10"
  filename         = "${var.file_name}"
  source_code_hash = "${base64sha256(file("${var.file_name}"))}"
  role             = "${aws_iam_role.distribute.arn}"
  memory_size      = 256
  timeout          = 30

  tracing_config {
    mode = "Active"
  }

  dead_letter_config {
    target_arn = "${lookup(var.topics["failed_events"], "arn")}"
  }

  environment {
    variables {
      import_course_flow    = "${lookup(var.flows["import_course"], "arn")}"
      process_bookmark_flow = "${lookup(var.flows["process_bookmark"], "arn")}"
      missing_bookmark_flow = "${lookup(var.flows["missing_bookmark"], "arn")}"
      process_course_flow   = "${lookup(var.flows["process_course"], "arn")}"
      index_courses_flow    = "${lookup(var.flows["index_courses"], "arn")}"
      remove_courses_flow   = "${lookup(var.flows["remove_courses"], "arn")}"
      save_status_flow      = "${lookup(var.flows["save_status"], "arn")}"
      save_statuses_flow    = "${lookup(var.flows["save_status"], "arn")}"
    }
  }
}
