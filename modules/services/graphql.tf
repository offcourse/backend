module graphql {
  source      = "./graphql"
  application = "${var.application}"
  file_name   = "${var.artifacts_dir}/graphql.zip"

  api_keys = {
    algolia_application_id = "${var.api_keys["algolia_application_id"]}"
    algolia_admin_key      = "${var.api_keys["algolia_admin_key"]}"
  }

  search_clusters = {
    data = "${var.search_clusters["data"]}"
  }


  buckets = {
    resources          = "${var.buckets["resources"]}"
    course_collections = "${var.buckets["course_collections"]}"
    temp               = "${var.buckets["temp"]}"
  }

  tables = {
    statuses   = "${var.tables["statuses"]}"
  }

  topics = {
    failed_events  = "${var.topics["failed_events"]}"
  }

  firehoses = {
    raw_events  = "${var.firehoses["raw_events"]}"
    stream_events  = "${var.firehoses["stream_events"]}"
  }

  policy_documents = {
    assume_role = "${data.aws_iam_policy_document.assume_role.json}"
    monitoring  = "${data.aws_iam_policy_document.monitoring.json}"
  }
}
