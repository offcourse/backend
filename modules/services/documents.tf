data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]

    principals = [
      {
        type = "Service"
        identifiers = [
          "lambda.amazonaws.com"
        ]
      },
    ]
  }
}

data "aws_iam_policy_document" "monitoring" {
  statement {
    actions = [
      "xray:PutTraceSegments",
      "xray:PutTelemetryRecords",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "*"
    ]
  }
}
