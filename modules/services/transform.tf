module transform {
  source      = "./transform"
  application = "${var.application}"
  file_name   = "${var.artifacts_dir}/transform-0.1.0-SNAPSHOT.zip"

  topics = {
    failed_events  = "${var.topics.["failed_events"]}"
  }

  policy_documents = {
    assume_role = "${data.aws_iam_policy_document.assume_role.json}"
    monitoring  = "${data.aws_iam_policy_document.monitoring.json}"
  }
}
