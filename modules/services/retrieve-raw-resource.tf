module retrieve_raw_resource {
  source      = "./retrieve-raw-resource"
  application = "${var.application}"
  file_name   = "${var.artifacts_dir}/retrieve-raw-resource.zip"


  topics = {
    failed_events  = "${var.topics["failed_events"]}"
  }

  policy_documents = {
    assume_role = "${data.aws_iam_policy_document.assume_role.json}"
    monitoring  = "${data.aws_iam_policy_document.monitoring.json}"
  }
}
