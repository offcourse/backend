module remove {
  source      = "./remove"
  application = "${var.application}"
  file_name   = "${var.artifacts_dir}/remove-0.1.0-SNAPSHOT.zip"

  api_keys = {
    algolia_application_id = "${var.api_keys["algolia_application_id"]}"
    algolia_admin_key = "${var.api_keys["algolia_admin_key"]}"
  }

  search_clusters = {
    data = "${var.search_clusters["data"]}"
  }

  topics = {
    failed_events  = "${var.topics.["failed_events"]}"
  }

  policy_documents = {
    assume_role = "${data.aws_iam_policy_document.assume_role.json}"
    monitoring  = "${data.aws_iam_policy_document.monitoring.json}"
  }
}
