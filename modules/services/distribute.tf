module distribute {
  source      = "./distribute"
  application = "${var.application}"
  file_name   = "${var.artifacts_dir}/distribute-0.1.0-SNAPSHOT.zip"

  tables = {
    courses = "${var.tables["courses"]}"
    statuses = "${var.tables["statuses"]}"
  }

  buckets = {
    temp      = "${var.buckets["temp"]}"
    bookmarks = "${var.buckets["bookmarks"]}"
  }

  flows = {
    import_course    = "${var.flows["import_course"]}"
    process_bookmark = "${var.flows["process_bookmark"]}"
    missing_bookmark = "${var.flows["missing_bookmark"]}"
    process_course   = "${var.flows["process_course"]}"
    save_status      = "${var.flows["save_status"]}"
    index_courses    = "${var.flows["index_courses"]}"
    remove_courses   = "${var.flows["remove_courses"]}"
  }

  topics = {
    failed_events  = "${var.topics.["failed_events"]}"
  }

  policy_documents = {
    assume_role = "${data.aws_iam_policy_document.assume_role.json}"
    monitoring  = "${data.aws_iam_policy_document.monitoring.json}"
  }
}
