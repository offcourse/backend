module authorize {
  source      = "./authorize"
  application = "${var.application}"
  file_name   = "${var.artifacts_dir}/authorize.zip"

  topics = {
    failed_events  = "${var.topics.["failed_events"]}"
  }

  auth = {
    user_pool = "${var.auth["user_pool"]}"
  }

  policy_documents = {
    assume_role = "${data.aws_iam_policy_document.assume_role.json}"
    monitoring  = "${data.aws_iam_policy_document.monitoring.json}"
  }
}
