resource "aws_lambda_function" "graphql" {
  function_name    = "${var.application}-graphql"
  handler          = "index.handler"
  runtime          = "nodejs6.10"
  filename         = "${var.file_name}"
  source_code_hash = "${base64sha256(file("${var.file_name}"))}"
  role             = "${aws_iam_role.graphql.arn}"
  memory_size      = 256
  timeout          = 60
  publish          = true

  tracing_config {
    mode = "Active"
  }

  dead_letter_config {
    target_arn = "${lookup(var.topics["failed_events"], "arn")}"
  }

  environment {
    variables {
      algolia_application_id    = "${var.api_keys["algolia_application_id"]}"
      algolia_admin_key         = "${var.api_keys["algolia_admin_key"]}"
      search_cluster            = "${lookup(var.search_clusters["data"], "endpoint")}"
      temp_bucket               = "${lookup(var.buckets["temp"], "name")}"
      resources_bucket          = "${lookup(var.buckets["resources"], "name")}"
      course_collections_bucket = "${lookup(var.buckets["course_collections"], "name")}"
      statuses_table            = "${lookup(var.tables["statuses"], "name")}"
      raw_events_stream         = "${lookup(var.firehoses["raw_events"], "name")}"
      processed_events_stream   = "${lookup(var.firehoses["stream_events"], "name")}"
    }
  }
}
