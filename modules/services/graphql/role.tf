resource "aws_iam_role" "graphql" {
  name               = "${var.application}-graphql_service"
  assume_role_policy = "${var.policy_documents["assume_role"]}"
}

resource "aws_iam_role_policy" "resources_read" {
  name   = "resources_read"
  role   = "${aws_iam_role.graphql.name}"
  policy = "${lookup(var.buckets["resources"], "read_policy")}"
}
resource "aws_iam_role_policy" "course_collections_read" {
  name   = "course_collections_read"
  role   = "${aws_iam_role.graphql.name}"
  policy = "${lookup(var.buckets["course_collections"], "read_policy")}"
}

resource "aws_iam_role_policy" "statuses_table_read" {
  name   = "statuses_table_read"
  role   = "${aws_iam_role.graphql.name}"
  policy = "${lookup(var.tables["statuses"], "read_policy")}"
}

resource "aws_iam_role_policy" "statuses_table_write" {
  name   = "statuses_table_write"
  role   = "${aws_iam_role.graphql.name}"
  policy = "${lookup(var.tables["statuses"], "write_policy")}"
}

resource "aws_iam_role_policy" "search_cluster_read" {
  name   = "search_cluster_read"
  role   = "${aws_iam_role.graphql.name}"
  policy = "${lookup(var.search_clusters["data"], "read_policy")}"
}

resource "aws_iam_role_policy" "temp_write" {
  name   = "temp_write"
  role   = "${aws_iam_role.graphql.name}"
  policy = "${lookup(var.buckets["temp"], "write_policy")}"
}

resource "aws_iam_role_policy" "raw_events_full" {
  name   = "raw_events_full"
  role   = "${aws_iam_role.graphql.name}"
  policy = "${lookup(var.firehoses["raw_events"], "full_policy")}"
}

resource "aws_iam_role_policy" "stream_events_full" {
  name   = "stream_events_full"
  role   = "${aws_iam_role.graphql.name}"
  policy = "${lookup(var.firehoses["stream_events"], "full_policy")}"
}

resource "aws_iam_role_policy" "failed_events_publish" {
  name   = "failed_events"
  role   = "${aws_iam_role.graphql.name}"
  policy = "${lookup(var.topics["failed_events"], "publish_policy")}"
}

resource "aws_iam_role_policy" "monitoring" {
  name   = "monitoring"
  role   = "${aws_iam_role.graphql.id}"
  policy = "${var.policy_documents["monitoring"]}"
}
