resource "aws_lambda_alias" "graphql_production" {
  name             = "production"
  description      = "the alias for our production env"
  function_name    = "${aws_lambda_function.graphql.arn}"
  function_version = "50"
} 