output info {
  value = {
    arn = "${aws_lambda_function.graphql.arn}"
    production_arn = "${aws_lambda_alias.graphql_production.arn}"
    role_arn = "${aws_iam_role.graphql.arn}"
  }
}
