resource "aws_iam_role" "retrieve" {
  name = "${var.application}-retrieve_service"
  assume_role_policy = "${var.policy_documents["assume_role"]}"
}
resource "aws_iam_role_policy" "courses_table_read" {
  name   = "courses_table_read"
  role   = "${aws_iam_role.retrieve.name}"
  policy = "${lookup(var.tables["courses"], "read_policy")}",
}

resource "aws_iam_role_policy" "raw_resources_read" {
  name   = "raw_resources_read"
  role   = "${aws_iam_role.retrieve.name}"
  policy = "${lookup(var.buckets["raw_resources"], "read_policy")}"
}

resource "aws_iam_role_policy" "resources_read" {
  name   = "resources_read"
  role   = "${aws_iam_role.retrieve.name}"
  policy = "${lookup(var.buckets["resources"], "read_policy")}"
}

resource "aws_iam_role_policy" "failed_events_publish" {
  name   = "failed_events"
  role   = "${aws_iam_role.retrieve.name}"
  policy = "${lookup(var.topics["failed_events"], "publish_policy")}"
}

resource "aws_iam_role_policy" "monitoring" {
  name   =  "monitoring"
  role   = "${aws_iam_role.retrieve.id}"
  policy = "${var.policy_documents["monitoring"]}"
}
