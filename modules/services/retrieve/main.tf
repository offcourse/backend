resource "aws_lambda_function" "retrieve" {
  function_name    = "${var.application}-retrieve"
  handler          = "index.handler"
  runtime          = "nodejs6.10"
  filename         = "${var.file_name}"
  source_code_hash = "${base64sha256(file("${var.file_name}"))}"
  role             = "${aws_iam_role.retrieve.arn}"
  memory_size      = 256
  timeout          = 60

  tracing_config {
    mode = "Active"
  }

  dead_letter_config {
    target_arn = "${lookup(var.topics["failed_events"], "arn")}"
  }

  environment {
    variables {
      github_api_key       = "${var.api_keys["github"]}"
      mercury_api_key      = "${var.api_keys["mercury"]}"
      raw_resources_bucket = "${lookup(var.buckets["raw_resources"], "name")}"
      resources_bucket     = "${lookup(var.buckets["resources"], "name")}"
      courses_table        = "${lookup(var.tables["courses"], "name")}"
    }
  }
}
