variable application {}
variable file_name {}

variable api_keys {
  type = "map"
}

variable tables {
  type = "map"
}

variable buckets {
  type = "map"
}

variable search_clusters {
  type = "map"
}

variable topics {
  type = "map"
}
variable policy_documents {
  type = "map"

}
