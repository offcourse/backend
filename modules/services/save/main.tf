resource "aws_lambda_function" "save" {
  function_name    = "${var.application}-save"
  handler          = "index.handler"
  runtime          = "nodejs6.10"
  filename         = "${var.file_name}"
  source_code_hash = "${base64sha256(file("${var.file_name}"))}"
  role             = "${aws_iam_role.save.arn}"
  memory_size      = 256
  timeout          = 60

  tracing_config {
    mode = "Active"
  }

  dead_letter_config {
    target_arn = "${lookup(var.topics["failed_events"], "arn")}"
  }

  environment {
    variables {
      algolia_application_id = "${var.api_keys["algolia_application_id"]}"
      algolia_admin_key      = "${var.api_keys["algolia_admin_key"]}"
      search_cluster         = "${lookup(var.search_clusters["data"], "endpoint")}"
      assets_bucket          = "${lookup(var.buckets["assets"], "name")}"
      raw_resources_bucket   = "${lookup(var.buckets["raw_resources"], "name")}"
      bookmarks_bucket       = "${lookup(var.buckets["bookmarks"], "name")}"
      resources_bucket       = "${lookup(var.buckets["resources"], "name")}"
      statuses_bucket        = "${lookup(var.buckets["statuses"], "name")}"
      temp_bucket            = "${lookup(var.buckets["temp"], "name")}"
      courses_table          = "${lookup(var.tables["courses"], "name")}"
      statuses_table         = "${lookup(var.tables["statuses"], "name")}"
    }
  }
}
