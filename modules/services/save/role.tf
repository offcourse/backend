resource "aws_iam_role" "save" {
  name = "${var.application}-save_service"
  assume_role_policy = "${var.policy_documents["assume_role"]}"
}
resource "aws_iam_role_policy" "courses_table_write" {
  name   = "courses_table_write"
  role   = "${aws_iam_role.save.name}"
  policy = "${lookup(var.tables["courses"], "write_policy")}",
}

resource "aws_iam_role_policy" "statuses_table_write" {
  name   = "statuses_table_write"
  role   = "${aws_iam_role.save.name}"
  policy = "${lookup(var.tables["statuses"], "write_policy")}",
}

resource "aws_iam_role_policy" "raw_resources_write" {
  name   = "raw_resources_write"
  role   = "${aws_iam_role.save.name}"
  policy = "${lookup(var.buckets["raw_resources"], "write_policy")}"
}

resource "aws_iam_role_policy" "statuses_write" {
  name   = "statuses_write"
  role   = "${aws_iam_role.save.name}"
  policy = "${lookup(var.buckets["statuses"], "write_policy")}"
}

resource "aws_iam_role_policy" "bookmarks_write" {
  name   = "bookmarks_write"
  role   = "${aws_iam_role.save.name}"
  policy = "${lookup(var.buckets["bookmarks"], "write_policy")}"
}

resource "aws_iam_role_policy" "resources_write" {
  name   = "resources_write"
  role   = "${aws_iam_role.save.name}"
  policy = "${lookup(var.buckets["resources"], "write_policy")}"
}

resource "aws_iam_role_policy" "search_cluster_write" {
  name   = "search_cluster_write"
  role   = "${aws_iam_role.save.name}"
  policy = "${lookup(var.search_clusters["data"], "write_policy")}"
}

resource "aws_iam_role_policy" "temp_write" {
  name   = "temp_write"
  role   = "${aws_iam_role.save.name}"
  policy = "${lookup(var.buckets["temp"], "write_policy")}"
}

resource "aws_iam_role_policy" "assets_write" {
  name   = "assets_write"
  role   = "${aws_iam_role.save.name}"
  policy = "${lookup(var.buckets["assets"], "write_policy")}"
}

resource "aws_iam_role_policy" "failed_events_publish" {
  name   = "failed_events_publish"
  role   = "${aws_iam_role.save.name}"
  policy = "${lookup(var.topics["failed_events"], "publish_policy")}"
}

resource "aws_iam_role_policy" "monitoring" {
  name = "monitoring"
  role = "${aws_iam_role.save.id}"
  policy = "${var.policy_documents["monitoring"]}"
}
