output info {
  value = {
    arn = "${aws_lambda_function.retrieve_raw_resource.arn}"
    role_arn = "${aws_iam_role.retrieve_raw_resource.arn}"
  }
}
