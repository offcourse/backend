resource "aws_iam_role" "retrieve_raw_resource" {
  name               = "${var.application}-retrieve_raw_resource_service"
  assume_role_policy = "${var.policy_documents["assume_role"]}"
}

resource "aws_iam_role_policy" "monitoring" {
  name   = "monitoring"
  role   = "${aws_iam_role.retrieve_raw_resource.id}"
  policy = "${var.policy_documents["monitoring"]}"
}

resource "aws_iam_role_policy" "failed_events_publish" {
  name   = "failed_events"
  role   = "${aws_iam_role.retrieve_raw_resource.name}"
  policy = "${lookup(var.topics["failed_events"], "publish_policy")}"
}