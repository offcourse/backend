resource "aws_lambda_function" "retrieve_raw_resource" {
  function_name    = "${var.application}-retrieve-raw-resource"
  handler          = "index.handler"
  runtime          = "nodejs6.10"
  filename         = "${var.file_name}"
  source_code_hash = "${base64sha256(file("${var.file_name}"))}"
  role             = "${aws_iam_role.retrieve_raw_resource.arn}"
  memory_size      = 256
  timeout          = 60
  publish          = true

  tracing_config {
    mode = "Active"
  }

  dead_letter_config {
    target_arn = "${lookup(var.topics["failed_events"], "arn")}"
  }
}
