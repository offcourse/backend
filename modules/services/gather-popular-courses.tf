module gather_popular_courses {
  source      = "./gather-popular-courses"
  application = "${var.application}"
  file_name   = "${var.artifacts_dir}/gather-popular-courses.zip"

  search_clusters = {
    data = "${var.search_clusters["data"]}"
  }


  buckets = {
    big_table_artifacts = "${var.buckets["big_table_artifacts"]}"
    stream_events = "${var.buckets["stream_events"]}"
  }

  topics = {
    failed_events  = "${var.topics["failed_events"]}"
  }

  big_tables = {
    events  = "${var.big_tables["events"]}"
  }


  policy_documents = {
    assume_role = "${data.aws_iam_policy_document.assume_role.json}"
    monitoring  = "${data.aws_iam_policy_document.monitoring.json}"
  }
}
