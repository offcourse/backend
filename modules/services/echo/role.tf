resource "aws_iam_role" "echo" {
  name = "${var.application}-echo_service"
  assume_role_policy = "${var.policy_documents["assume_role"]}"
}

resource "aws_iam_role_policy" "failed_events_publish" {
  name   = "${var.application}-failed_events_publish_policy"
  role   = "${aws_iam_role.echo.name}"
  policy = "${lookup(var.topics["failed_events"], "publish_policy")}"
}

resource "aws_iam_role_policy" "monitoring" {
  name = "${var.application}-monitoring_policy"
  role = "${aws_iam_role.echo.id}"
  policy = "${var.policy_documents["monitoring"]}"
}
