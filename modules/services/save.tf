module save {
  source      = "./save"
  application = "${var.application}"
  file_name   = "${var.artifacts_dir}/save-0.1.0-SNAPSHOT.zip"

  api_keys = {
    algolia_application_id = "${var.api_keys["algolia_application_id"]}"
    algolia_admin_key = "${var.api_keys["algolia_admin_key"]}"
  }

  search_clusters = {
    data = "${var.search_clusters["data"]}"
  }

  tables = {
    courses    = "${var.tables["courses"]}"
    statuses   = "${var.tables["statuses"]}"
  }

  buckets = {
    raw_resources = "${var.buckets["raw_resources"]}"
    statuses      = "${var.buckets["statuses"]}"
    resources     = "${var.buckets["resources"]}"
    bookmarks     = "${var.buckets["bookmarks"]}"
    temp          = "${var.buckets["temp"]}"
    assets        = "${var.buckets["assets"]}"
  }

  topics = {
    failed_events  = "${var.topics.["failed_events"]}"
  }

  policy_documents = {
    assume_role = "${data.aws_iam_policy_document.assume_role.json}"
    monitoring  = "${data.aws_iam_policy_document.monitoring.json}"
  }
}
