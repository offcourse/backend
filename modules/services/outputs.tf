output save {
  value = "${module.save.info}"
}

output transform {
  value = "${module.transform.info}"
}

output distribute {
  value = "${module.distribute.info}"
}

output retrieve {
  value = "${module.retrieve.info}"
}

output authorize {
  value = "${module.authorize.info}"
}

output echo {
  value = "${module.echo.info}"
}

output remove {
  value = "${module.remove.info}"
}

output graphql {
  value = "${module.graphql.info}"
}
