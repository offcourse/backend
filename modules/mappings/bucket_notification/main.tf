variable application {}

variable bucket {
  type = "map"
}

variable service {
  type = "map"
}

resource "aws_s3_bucket_notification" "temp" {
  bucket = "${var.bucket["name"]}"

  lambda_function {
    lambda_function_arn = "${var.service["arn"]}"
    events              = ["s3:ObjectCreated:*"]
  }
}

resource "aws_lambda_permission" "allow_invocation" {
  statement_id  = "${var.application}-${var.bucket["name"]}"
  action        = "lambda:InvokeFunction"
  function_name = "${var.service["arn"]}"
  principal     = "s3.amazonaws.com"
  source_arn    = "${var.bucket["arn"]}"
}
