variable application {}

variable buckets  {
  type = "map"
}

variable services {
  type = "map"
}

variable tables {
  type = "map"
}
