resource "aws_lambda_event_source_mapping" "courses" {
  batch_size = 10
  event_source_arn = "${lookup(var.tables["courses"], "stream_arn")}"
  enabled = true
  function_name = "${lookup(var.services["distribute"], "arn")}"
  starting_position = "LATEST"
}

resource "aws_lambda_event_source_mapping" "statuses" {
  batch_size = 10
  event_source_arn = "${lookup(var.tables["statuses"], "stream_arn")}"
  enabled = false
  function_name = "${lookup(var.services["distribute"], "arn")}"
  starting_position = "LATEST"
}

module temp_notifications {
  source       = "./bucket_notification"
  application  = "${var.application}"

  bucket  = "${var.buckets["temp"]}"
  service = "${var.services["distribute"]}"
}

module bookmarks_notifications {
  source       = "./bucket_notification"
  application  = "${var.application}"

  bucket  = "${var.buckets["bookmarks"]}"
  service = "${var.services["distribute"]}"
}
