resource "aws_cloudfront_distribution" "default" {
  aliases = [
    "${slice(concat(local.base_alias, local.extended_aliases),
        0, var.needs_prefix ? length(local.base_alias) : length(local.base_alias) + length(local.extended_aliases))}"
  ]

  origin {
    domain_name = "${aws_s3_bucket.website.website_endpoint}"
    origin_id   = "${var.application}-${var.site_name}"

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1"]
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  custom_error_response {
    error_code         = 404
    response_code      = 200
    response_page_path = "/index.html"
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${var.application}-${var.site_name}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type   = "viewer-request"
      lambda_arn   = "${lookup(var.edge_functions["redirect"], "qualified_arn")}"
      include_body = false
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags {
    Application = "${var.application}"
    PageName    = "${var.site_name}"
  }

  viewer_certificate {
    acm_certificate_arn      = "${var.zone["certificate_arn"]}"
    minimum_protocol_version = "TLSv1"
    ssl_support_method       = "sni-only"
  }
}
