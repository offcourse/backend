data "aws_iam_policy_document" "write" {
  statement {
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "${aws_s3_bucket.website.arn}/*"
    ]
  }
}

data "aws_iam_policy_document" "read" {
  statement {
    actions = [
      "s3:getObject"
    ]
    resources = [
      "${aws_s3_bucket.website.arn}",
      "${aws_s3_bucket.website.arn}/*"
    ]
  }
}

data "aws_iam_policy_document" "website" {
  statement {
    principals = [
      {
        type        = "AWS"
        identifiers = ["*"]
      },
    ]

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "${aws_s3_bucket.website.arn}/*"
    ]
  }
}
