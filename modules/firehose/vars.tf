variable application {}
variable bucket {
    type = "map"
}
variable stream_name {}

locals {
  stream_name = "${var.application}-${var.stream_name}"
}