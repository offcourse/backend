data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]

    principals = [
      {
        type = "Service"
        identifiers = [
          "firehose.amazonaws.com",
          "lambda.amazonaws.com",
          "s3.amazonaws.com",
        ]
      },
    ]
  }
}