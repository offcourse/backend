resource "aws_iam_role" "firehose" {
  name = "${var.application}-${var.stream_name}-firehose_role"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role.json}"
}

resource "aws_iam_role_policy" "bucket_full" {
  name   = "${var.application}-${var.stream_name}-bucket_full"
  role   = "${aws_iam_role.firehose.name}"
  policy = "${lookup(var.bucket, "full_policy")}"
}