output info {
  value = {
    name          = "${local.stream_name}"
    arn           = "${aws_kinesis_firehose_delivery_stream.firehose.arn}"
    full_policy   = "${module.firehose_policies.info["full"]}"
  }

}