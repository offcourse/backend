data "aws_iam_policy_document" "full" {
  statement {
    actions = [
      "firehose:DeleteDeliveryStream",
      "firehose:PutRecord",
      "firehose:PutRecordBatch",
      "firehose:UpdateDestination"
    ]
    resources = [
      "${var.firehose_arn}"
    ]
  }
}