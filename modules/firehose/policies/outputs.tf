output "info" {
  value = {
    full   = "${data.aws_iam_policy_document.full.json}"
  }
}
