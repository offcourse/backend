
resource "aws_iam_policy" "full" {
  name   = "${var.application}-${var.firehose_name}_firehose_full"
  policy = "${data.aws_iam_policy_document.full.json}"
}
