resource "aws_kinesis_firehose_delivery_stream" "firehose" {
  name        = "${local.stream_name}"
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn       = "${aws_iam_role.firehose.arn}"
    bucket_arn     = "${lookup(var.bucket, "arn")}"
  }
}

module firehose_policies {
  source        = "./policies"
  application   = "${var.application}"
  firehose_name = "${var.stream_name}"
  firehose_arn  = "${aws_kinesis_firehose_delivery_stream.firehose.arn}"
}
