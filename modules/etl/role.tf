
resource "aws_iam_role" "etl" {
  name = "${var.application}-etl-role"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role.json}"
}

resource "aws_iam_role_policy" "etl-monitoring" {
  name = "${var.application}-etl-monitoring"
  role = "${aws_iam_role.etl.id}"
  policy = "${data.aws_iam_policy_document.monitoring.json}"
}

resource "aws_iam_role_policy" "etl-crawler" {
  name = "${var.application}-etl-crawler"
  role = "${aws_iam_role.etl.id}"
  policy = "${data.aws_iam_policy_document.crawler.json}"
}

resource "aws_iam_role_policy" "stream_events_bucket_full" {
  name   = "${var.application}-stream-events-bucket_full"
  role   = "${aws_iam_role.etl.name}"
  policy = "${lookup(var.buckets["stream_events"], "full_policy")}"
}
resource "aws_iam_role_policy" "course_collections_bucket_full" {
  name   = "${var.application}-course-collections-bucket_full"
  role   = "${aws_iam_role.etl.name}"
  policy = "${lookup(var.buckets["course_collections"], "full_policy")}"
}

resource "aws_iam_role_policy" "courses_read" {
  name   = "${var.application}-courses_read"
  role   = "${aws_iam_role.etl.name}"
  policy = "${lookup(var.tables["courses"], "read_policy")}"
}

