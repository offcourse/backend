resource "aws_glue_catalog_database" "offcourse_database" {
  name = "${var.application}-database"
}

data "template_file" "crawler_configuration" {
  template = <<JSON
    {
    "Version": 1.0,
    "Grouping": { "TableGroupingPolicy": "CombineCompatibleSchemas" },
    "CrawlerOutput": {
      "Partitions": { "AddOrUpdateBehavior": "InheritFromTable" }
   }
  }
  JSON
}
resource "aws_glue_crawler" "json_event_stream" {
  database_name = "${aws_glue_catalog_database.offcourse_database.name}"
  name          = "json_event_stream"
  role          = "${aws_iam_role.etl.arn}"

  configuration = "${data.template_file.crawler_configuration.template}"

  schedule      = "cron(15 12 * * ? *)"

  s3_target {
    path = "s3://${local.stream_events_bucket_name}"
  }
}
