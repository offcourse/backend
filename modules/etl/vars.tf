variable application {}
variable buckets {
    type = "map"
}
variable tables {
    type = "map"
}

locals {
  stream_events_bucket_name = "${lookup(var.buckets["stream_events"], "name")}"
}
