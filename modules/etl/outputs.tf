output "info" {
  value = {
    database_name = "${aws_glue_catalog_database.offcourse_database.id}"
    full_policy  = "${data.aws_iam_policy_document.full.json}"
  }
}
