data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]

    principals = [
      {
        type = "Service"
        identifiers = [
          "glue.amazonaws.com",
        ]
      },
    ]
  }
}

data "aws_iam_policy_document" "monitoring" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "glue:CreateDatabase"
    ]
    resources = [
      "arn:aws:logs:*:*:/aws-glue/*"
    ]
  }
}

data "aws_iam_policy_document" "crawler" {
  statement {
    actions = [
      "glue:*"
    ]
    resources = [
      "*"
    ]
  }
}

data "aws_iam_policy_document" "full" {
  statement {
    actions = [
      "athena:*",
      "glue:*"
    ]
    resources = [
      "*"
    ]
  }
}