resource "aws_s3_bucket" "website" {
  bucket        = "${var.application}-${var.site_name}"
  acl           = "public-read"

  website {
    index_document = "index.html"
    error_document = "index.html"
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    expose_headers = ["ETag"]
    max_age_seconds = 3000
  }

  tags {
    Application = "${var.application}"
    PageName    = "${var.site_name}"
  }
}
