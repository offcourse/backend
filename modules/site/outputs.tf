output "info" {
  value = {
    endpoint       = "${aws_cloudfront_distribution.default.domain_name}"
    hosted_zone_id = "${aws_cloudfront_distribution.default.hosted_zone_id}"
    domain_name    = "${aws_cloudfront_distribution.default.domain_name}"
    name           = "${aws_s3_bucket.website.id}"
    arn            = "${aws_s3_bucket.website.arn}"
    website_policy = "${data.aws_iam_policy_document.website.json}"
    read_policy    = "${data.aws_iam_policy_document.read.json}"
    write_policy   = "${data.aws_iam_policy_document.write.json}"
  }
}
