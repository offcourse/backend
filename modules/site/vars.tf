variable site_name {}

variable needs_prefix {
  default = true
}
variable redirect {
  default = false
}

variable application {}

variable zone {
  type = "map"
}
variable edge_functions {
  type = "map"
  default = {}
}


locals {
  full_name    = "${var.site_name}.${var.zone["domain_name"]}"
  site_name    = "${!var.needs_prefix ?  var.zone["domain_name"] : local.full_name}"
  domain_alias = "www.${var.zone["domain_name"]}"
  app_alias    = "${var.needs_prefix ? local.site_name : "stable.${var.zone["domain_name"]}"}"
  base_alias = ["${local.app_alias}"]
  extended_aliases = ["${local.site_name}", "${local.domain_alias}"]
}
